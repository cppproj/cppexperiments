//============================================================================
// Name        : Any.cpp
// Created on  : 01.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : std::any class tests
//============================================================================

#include <iostream>
#include <string>

#include <any>
#include <map>

#include "Any.h"
#include "../Integer/Integer.h"
#include "../TestSupport/TestSupport.h"

namespace Any_Tests {

	template<typename Type>
	void PrintAny(const std::any& var) {
		try {
			std::cout << std::any_cast<Type>(var);
			std::cout << "         [Type: " << typeid(Type).name() << "]" << std::endl;
		}
		catch (const std::bad_any_cast& exception) {
			std::cout << "Failed to cast std::variable to " << typeid(Type).name() << std::endl;
			std::cout << exception.what() << std::endl;
		}
	}

	void CreateAndPrint() {
		std::any var(111);

		PrintAny<int>(var);
		PrintAny<String>(var);
		PrintAny<Integer>(var);

		std::cout << "\n------------------------------\n" << std::endl;

		std::any var_str(String("Hello!"));

		PrintAny<String>(var_str);
		PrintAny<int>(var_str);
		PrintAny<Integer>(var_str);
	}

	void LittleDemo() {
		std::any variable(12);
		PrintAny<int>(variable);

		variable = String("Hello!");
		PrintAny<String>(variable);

		variable = 16;

		PrintAny<int>(variable);
		PrintAny<String>(variable);

		std::cout << "\n>>> reset()  \n" << std::endl;

		/** reset and check if it contains any value. **/
		variable.reset();
		if (false == variable.has_value()) {
			std::cout << "a is empty!" << "\n";
		}

		std::cout << "\n------------------------------\n" << std::endl;

		// you can use it in a container:
		std::map<String, std::any> m;
		m["integer"] = 10;
		m["string"] = String("Hello World");
		m["float"] = 1.0f;

		for (auto &[key, val] : m) {
			if (val.type() == typeid(int))
				std::cout << "int: " << std::any_cast<int>(val) << "\n";
			else if (val.type() == typeid(String))
				std::cout << "string: " << std::any_cast<String>(val) << "\n";
			else if (val.type() == typeid(float))
				std::cout << "float: " << std::any_cast<float>(val) << "\n";
		}
	}

	void EmplaceTests_Fast_NoCopyConstructor() {
		std::any anyVar;

		anyVar.emplace<TestSupport::FirstType>("FirstTypeTestValue_1");

		try {
			auto& var = std::any_cast<TestSupport::FirstType&>(anyVar);
			std::cout << "VALUE: " << var.getValue() << std::endl;
		}
		catch (const std::bad_any_cast& exc) {
			std::cout << "anyVar variable can not be cast to FirstType type variable.\n" << exc.what() << std::endl;
		}

		anyVar.emplace<TestSupport::SecondType>("SecondTypeTestValue_1");

		try {
			auto& var = std::any_cast<TestSupport::SecondType&>(anyVar);
			std::cout << "VALUE: " << var.getValue() << std::endl;
		}
		catch (const std::bad_any_cast& exc) {
			std::cout << "anyVar variable can not be cast to SecondType type variable.\n" << exc.what() << std::endl;
		}
	}

	void EmplaceTests_Fast_NoCopyConstructor_Ptr() {
		std::any anyVar;

		anyVar.emplace<TestSupport::FirstType>("FirstTypeTestValue_1");

		try {
			auto* var = std::any_cast<TestSupport::FirstType>(&anyVar);
			std::cout << "VALUE: " << var->getValue() << std::endl;
		}
		catch (const std::bad_any_cast& exc) {
			std::cout << "anyVar variable can not be cast to FirstType type variable.\n" << exc.what() << std::endl;
		}

		anyVar.emplace<TestSupport::SecondType>("SecondTypeTestValue_1");

		try {
			auto* var = std::any_cast<TestSupport::SecondType>(&anyVar);
			std::cout << "VALUE: " << var->getValue() << std::endl;
		}
		catch (const std::bad_any_cast& exc) {
			std::cout << "anyVar variable can not be cast to SecondType type variable.\n" << exc.what() << std::endl;
		}
	}

	void EmplaceTests_BAD() {
		std::cout << "************************** BAD: **********************\n" << std::endl;
		{
			std::any anyVar(TestSupport::FirstType("TEST1"));
			try {
				TestSupport::FirstType var = std::any_cast<TestSupport::FirstType>(anyVar);
				std::cout << "VALUE: " << var.getValue() << std::endl;
			}
			catch (const std::bad_any_cast& exc) {
				std::cout << "anyVar variable can not be cast to FirstType type variable" << std::endl;
				std::cout << exc.what() << std::endl;
			}

			anyVar.emplace<TestSupport::SecondType>(TestSupport::SecondType("TEST2"));
			try {
				TestSupport::SecondType var = std::any_cast<TestSupport::SecondType>(anyVar);
				std::cout << "VALUE: " << var.getValue() << std::endl;
			}
			catch (const std::bad_any_cast& exc) {
				std::cout << "anyVar variable can not be cast to SecondType type variable" << std::endl;
				std::cout << exc.what() << std::endl;
			}
		}
		std::cout << "\n************************** GOOD: **********************\n" << std::endl;
		{
			std::any anyVar;
			anyVar.emplace<TestSupport::FirstType>("FirstType_Variable");
			try {
				TestSupport::FirstType& var = std::any_cast<TestSupport::FirstType&>(anyVar);
				std::cout << "VALUE: " << var.getValue() << std::endl;
			}
			catch (const std::bad_any_cast& exc) {
				std::cout << "anyVar variable can not be cast to FirstType type variable" << std::endl;
				std::cout << exc.what() << std::endl;
			}

			anyVar.emplace<TestSupport::SecondType>("SecondType_Variable");
			try {
				TestSupport::SecondType& var = std::any_cast<TestSupport::SecondType&>(anyVar);
				std::cout << "VALUE: " << var.getValue() << std::endl;
			}
			catch (const std::bad_any_cast& exc) {
				std::cout << "anyVar variable can not be cast to SecondType type variable" << std::endl;
				std::cout << exc.what() << std::endl;
			}
		}
	}

	void Swap()
	{
		std::any integer1 = std::make_any<Integer>(111);
		std::any integer2 = std::make_any<Integer>(222);

		std::cout << "integer1 = " << std::any_cast<Integer&>(integer1) << std::endl;
		std::cout << "integer2 = " << std::any_cast<Integer&>(integer2) << std::endl;

		integer2.swap(integer1);

		std::cout << "integer1 = " << std::any_cast<Integer&>(integer1) << std::endl;
		std::cout << "integer2 = " << std::any_cast<Integer&>(integer2) << std::endl;
	}

	void Reset()
	{
		std::any integer1 = std::make_any<Integer>(111);
		integer1.reset();

		std::cout << "EXIT" << std::endl;
	}

	void Has_Value() {
		std::boolalpha(std::cout);

		std::any a0;
		std::cout << "a0.has_value(): " << a0.has_value() << "\n\n";

		std::any a1 = 42;
		std::cout << "a1.has_value(): " << a1.has_value() << '\n';
		std::cout << "a1 = " << std::any_cast<int>(a1) << '\n';
		a1.reset();
		std::cout << "a1.has_value(): " << a1.has_value() << '\n';
	}

	template<typename T>
	void print(const std::any & variable) {
		try {
			auto var = std::any_cast<T>(variable);
			std::cout << "Value = "<< var << std::endl;
		}
		catch (...) {
			std::cout << "Faiied to cast variable to type '" << typeid(T).name() << "'" << std::endl;
		}
	};

	void __TEST__() {

		/*
		auto print1 = []<typename T>(const std::any& variable) {
			try {
				auto var = std::any_cast<T>(variable);
				std::cout << var << std::endl;
			} catch (...) {
				std::cout << "Faiied to cast variable to type '" << typeid(T).name() << "'" << std::endl;
			}
		};
		*/

		auto var = std::make_any<std::string>("Some_Text");
		std::cout << "var type: " << var.type().name() << ". has_value: " << std::boolalpha << var.has_value() << std::endl;
		print<std::string>(var);

		var = std::make_any<Integer>(123);
		std::cout << "\nvar type: " << var.type().name() << ". has_value: " << std::boolalpha << var.has_value() << std::endl;
		print<Integer>(var);

	}

}

namespace Any_Tests {
	void TEST_ALL()
	{
		// CreateAndPrint();
		// LittleDemo();

		// EmplaceTests_Fast_NoCopyConstructor();
		// EmplaceTests_Fast_NoCopyConstructor_Ptr();

		// EmplaceTests_BAD();

		// Swap();

		// Reset();

		// Has_Value();

		__TEST__();
	}
}