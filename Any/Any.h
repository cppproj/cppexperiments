//============================================================================
// Name        : Any.h
// Created on  : 01.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : std::any class tests
//============================================================================

#ifndef ANY_CLASS_TESTS_INCLUDE_GUARD_H_
#define ANY_CLASS_TESTS_INCLUDE_GUARD_H_

namespace Any_Tests {
	void TEST_ALL();
};

#endif /* ANY_CLASS_TESTS_INCLUDE_GUARD_H_ */
