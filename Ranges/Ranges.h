//============================================================================
// Name        : Ranges.h
// Created on  : 13.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Ranges tests
//============================================================================

#ifndef RANGES_TESTS_INCLUDE_GUARD_H_
#define RANGES_TESTS_INCLUDE_GUARD_H_

namespace Ranges {
	void TEST_ALL();
};

#endif /* RANGES_TESTS_INCLUDE_GUARD_H_ */
