//============================================================================
// Name        : Ranges.h
// Created on  : 13.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Ranges tests
//============================================================================

#define _CRT_SECURE_NO_WARNINGS

#include "Ranges.h"

#include <iostream>
#include <string>
#include <algorithm>
#include <array>
#include <vector>
#include <string_view>

#include <map>
#include <cstring>
#include <compare>
#include <ranges>
#include <span>
#include <bit>
#include <bitset>
#include <cstdint>
#include <iterator>
#include <initializer_list>
#include <concepts>
#include <span>

namespace Ranges {

    template <class V>
    void mutate(V& v) {
        v += 'A' - 'a';
    }

    template <class K, class V>
    void mutate_map_values(std::multimap<K, V>& m, K k) {
        auto iter_pair = m.equal_range(k);
        for (auto& [_, v] : std::ranges::subrange(iter_pair.first, iter_pair.second)) {
            mutate(v);
        }
    }

    void For_Each() {
        const std::vector<int> numbers{ 0,1,2,3,4,5,6,7,8,9 };
        auto print = [](int v) { std::cout << v << ' '; };
        std::ranges::for_each(numbers, print);
    }

    void SubRange()
    {
        auto print = [](std::string_view rem, auto const& mm) {
            std::cout << rem << "{ ";
            for (const auto& [k, v] : mm) std::cout << "{" << k << ",'" << v << "'} ";
            std::cout << "}\n";
        };

        std::multimap<int, char> mm{ {4,'a'}, {3,'-'}, {4,'b'}, {5,'-'}, {4,'c'} };
        print("Before: ", mm);
        mutate_map_values(mm, 4);
        print("After:  ", mm);
    }

    void size_test()
    {
        std::vector<int> v{ 1, 2, 3, 4, 5 };
        std::cout << "ranges::size(v) == " << std::ranges::size(v) << '\n';

        auto il = { 7 };
        std::cout << "ranges::size(il) == " << std::ranges::size(il) << '\n';

        int array[] = { 4, 5 }; // array has a known bound
        std::cout << "ranges::size(array) == " << std::ranges::size(array) << '\n';

        std::cout << std::boolalpha << "is_signed: " << std::is_signed_v<decltype(std::ranges::size(v))> << '\n';
    }

    void End()
    {
        std::vector<int> numbers = { 3, 1, 4 };
        namespace ranges = std::ranges;
        if (std::ranges::find(numbers, 5) != std::ranges::end(numbers)) {
            std::cout << "found a 5 in vector v!\n";
        }

        int a[] = { 5, 10, 15 };
        if (std::ranges::find(a, 5) != std::ranges::end(a)) {
            std::cout << "found a 5 in array a!\n";
        }
    }

    void Data()
    {
        std::string s{ "Hello world!\n" };

        char a[20]; // storage for a C-style string
        std::strcpy(a, std::ranges::data(s));
        std::cout << a;
    }


    template <std::ranges::input_range R>
    void print(R&& r)
    {
        if (std::ranges::empty(r)) {
            std::cout << "\tEmpty\n";
            return;
        }

        std::cout << "\tElements:";
        for (const auto& element : r) {
            std::cout << ' ' << element;
        }
        std::cout << '\n';
    }

    void Find_IF()
    {
        std::array<int, 6> data{ 6,5,4,3,2,1 };
        auto is_six = [](int v) -> bool { return 6 == v; };
        auto result = std::ranges::find_if(data, is_six);

        if (result != std::ranges::end(data)) {
            std::cout << "Result = " << *result << std::endl;
        }
    }

    void Filter_View()
    {
        std::array<int, 6> data{ 6,5,4,3,2,1 };
        auto is_six = [](int v) -> bool { return 6 == v; };
        for (int v : std::ranges::filter_view(data, is_six)) {
            std::cout << v << std::endl;
        }
    }

    void Filter_View_Vector()
    {
        const std::vector<int> vi{0,1,2,3,4,5,6,7,8,9};
        auto is_even = [](int v) -> bool { return 0 == v % 2; };

        std::cout << "\nConstruct filter 'on stack':\n";
        {
            std::ranges::filter_view evens(vi, is_even);
            for (auto v : evens)
                std::cout << v << ' ';
        }

        std::cout << "\nConstruct filter 'inplace':\n";
        {
            for (auto v : std::ranges::filter_view(vi, is_even))
                std::cout << v << ' ';
        }
    }

    void Filter()
    {
        const std::vector<int> numbers { 0,1,2,3,4,5,6,7,8,9 };
        auto is_even = [](int v) -> bool { return 0 == v % 2; };

        auto evens = numbers | std::ranges::views::filter(is_even);
        for (auto v : evens)
            std::cout << v << ' ';
    }

    void Filter_ForEach()
    {
        const std::vector<int> numbers{ 0,1,2,3,4,5,6,7,8,9 };
        auto is_even = [](int v) -> bool { return 0 == v % 2; };

        auto evens = numbers | std::ranges::views::filter(is_even);
        std::ranges::for_each(evens, [](auto v) { std::cout << v << ' '; });
    }


    void View_DropWhile()
    {
        const std::vector<int> numbers{ 0,1,2,3,4,5,6,7,8,9 };

        auto is_even = [](int v) -> bool { return 0 == v % 2; };
        auto print = [](int v) -> bool { std::cout << v << ' '; };

        //auto after_leading_event = std::ranges::drop_view(numbers, is_even);
        //std::ranges::for_each(after_leading_event, print);
    }

    void Take_View() {
        std::vector<int> numbers{ 0,1,2,3,4,5 };

        {
            std::ranges::take_view view{ numbers, 3 };
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }

        {
            auto view = std::ranges::take_view{ numbers, 3 };
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }

        {
            std::ranges::take_view view{ numbers, 35 };
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }
    }


    void Join_View() {
        std::vector<std::string> numbers{ "hello", " ", "ranges", " ", "world"  };
        /*
        for (char c : std::ranges::join_view{ numbers }) {
        }
        */
    }

    //---------------------------------------------------------------------------//

    void _TESTS_() {
        std::vector<int> numbers{ 0,1,2,3,4,5};

        {
            std::ranges::take_view view {numbers, 3};
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }

        {
            auto view = std::ranges::take_view{ numbers, 3 };
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }

        {
            std::ranges::take_view view{ numbers, 35 };
            std::ranges::for_each(view, [](auto v) { std::cout << v << ' '; });
            std::cout << std::endl;
        }
    }
}
	
namespace Ranges::Algoritms {

    auto is_even = [](int v) -> bool { return 0 == v % 2; };
    auto print = [](int v) { std::cout << v << ' '; };

    void Sort()
    {
        std::array<int, 5> array{5,4,3,2,1};
        std::ranges::sort(array);
        std::ranges::for_each(array, print);
    }

    void Sort_BackWards()
    {
        std::array<int, 5> array{ 5,4,3,2,1 };
        auto reverse_compare = [](int a, int b) {return a > b; };

        std::ranges::for_each(array, print);
        std::cout << std::endl;

        std::ranges::sort(array);

        std::ranges::for_each(array, print);
        std::cout << std::endl;

        std::ranges::sort(array, reverse_compare);

        std::ranges::for_each(array, print);
        std::cout << std::endl;
    }

    void Reverse() {
        const std::vector<int> numbers{ 0,1,2,3,4,5,6,7,8,9 };
        std::ranges::for_each(std::ranges::reverse_view(numbers), [](auto v) {
            std::cout << v << ' ';
        });
        std::cout << std::endl;
    }

    void Reverse_Span_Part() {
        auto print = [] (std::span<int> span) {
            std::ranges::for_each(std::ranges::reverse_view{ span }, [](const auto& v) {
                std::cout << v << ' ';
            });
            std::cout << std::endl;
        };

        std::vector<int> numbers{ 0,1,2,3,4,5,6,7,8,9 };
        for (int i = 0; auto _ : numbers) {
            print(std::span<int>(numbers).first(++i));
        }
        for (int i = numbers.size(); auto _ : numbers) {
            print(std::span<int>(numbers).first(--i));
        }
    }
}

void Ranges::TEST_ALL()
{
    // For_Each();

    // size_test();
    // empty_test();
    // End();
    // Data();
    // Find_IF();

    // Filter_View();
    // Filter_View_Vector();

    // Filter();
    // Filter_ForEach();

    // View_DropWhile(); // Not working

    // Take_View();
    // Join_View();

    // Algoritms::Sort();
    // Algoritms::Sort_BackWards();
    // Algoritms::Reverse();
    // Algoritms::Reverse_Span_Part();

    // _TESTS_();
}
