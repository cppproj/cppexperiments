//============================================================================
// Name        : BitwiseOperation.h
// Created on  : 07.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Bitwise operation tests
//============================================================================

#ifndef BITWISE_OPERATION_TESTS__H_
#define BITWISE_OPERATION_TESTS__H_

namespace BitwiseOperation {
	void TEST_ALL();
};

#endif /* BITWISE_OPERATION_TESTS__H_ */