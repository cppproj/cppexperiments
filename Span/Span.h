//============================================================================
// Name        : Span.h
// Created on  : 01.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Span tests
//============================================================================

#ifndef SPAN_TESTS_INCLUDE_GUARD_H_
#define SPAN_TESTS_INCLUDE_GUARD_H_

namespace Span {
	void TEST_ALL();
};

#endif /* SPAN_TESTS_INCLUDE_GUARD_H_ */
