//============================================================================
// Name        : Span.cpp
// Created on  : 01.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Span tests
//============================================================================

#include "Span.h"

#include <iostream>
#include <string>
#include <string_view>
#include <algorithm>

#include <array>
#include <span>
#include <list>
#include <vector>

namespace Span {

    template<typename T>
    std::ostream& operator<<(std::ostream& stream, std::span<T>& const span) {
        for (const auto& entry : span)
            stream << ' ' << entry;
        return stream;
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& stream, std::vector<T>& const vector) {
        for (const auto& entry : vector)
            stream << ' ' << entry;
        return stream;
    }

    template<class T, std::size_t N, std::size_t M> 
    [[nodiscard]]
    constexpr bool contains(std::span<T, N> span, std::span<T, M> sub) {
        return std::search(span.begin(), span.end(), sub.begin(), sub.end())
            != span.end();
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    //template<typename T>
    //void print(std::span<T> const data)  {
    void print(std::span<const int> const data) {
        for (auto offset{ 0U }; offset != data.size(); ++offset) {
            std::cout << data.subspan(offset).front() << ' ';
        }
        std::cout << '\n';
    }

    void Test()
    {
        constexpr int data[]{ 0, 1, 2, 3, 4, 5, 6 };
        print({ data, 4 });
    }

    ///////////////////////////////////////////////////////////////

    void Create() {
        std::vector<int> numbers{ 1,2,3,4,5,6,7,8,9 };

        {
            std::span<int> sp(numbers);
            std::cout << sp << std::endl;
        }
        
        std::cout << "-------------------------------------------- Test2 (constexpr): --------------------------------------------" << std::endl;

        {
            constexpr int data[]{ 0, 1, 2, 3, 4, 5, 6 };
            std::span<const int> sp(data);

            std::cout << sp << std::endl;
        }
    }

    void Create_Not() {
#if 0 
        std::list<int> numbers{ 1,2,3,4,5,6,7,8,9 };
        std::span<int> sp(numbers);
#endif
    }

 
    void Front() {
        constexpr int data[]{1, 2, 3, 4, 5, 6 };
        std::span<const int> sp(data);
        std::cout << sp.front() << std::endl;
    }

    void First() {
        constexpr int data[]{ 1, 2, 3, 4, 5, 6 };
        std::span<const int> sp(data);
        std::cout << "Source collection: " << sp << "\n\n";

        for (auto i : data) {
            auto s = sp.first(i);
            std::cout << "first (" << i << ") = " << s << std::endl;
        }
    }

    void Back() {
        constexpr int data[]{ 1, 2, 3, 4, 5, 6 };
        std::span<const int> sp(data);
        std::cout << sp.back() << std::endl;
    }

    void Last() {
        constexpr int data[]{ 1, 2, 3, 4, 5, 6 };
        std::span<const int> sp(data);
        std::cout << "Source collection: " << sp << "\n\n";

        for (auto i : data) {
            auto s = sp.last(i);
            std::cout << "last (" << i << ") = " << s << std::endl;
        }

        std::cout << "-------------------  dynamic_extent  --------------------------" << std::endl;

        for (auto i : data) {
            std::span<const int, std::dynamic_extent> s = sp.last(i);
            std::cout << "last (" << i << ") = " << s << std::endl;
        }

    }

    void Subspan() {
        constexpr int data[]{ 1, 2, 3, 4, 5, 6 };
        std::span<const int> data_span(data);

        {
            auto sub = data_span.subspan(2, 2);
            std::cout << "subspan(2, 2) = " << sub << std::endl;
        }
    }

    void Size() {
        std::array<int, 5> data{ 1,2,3,4,5 };
        std::span<int> data_span(data);

        std::cout << data_span << std::endl;
        std::cout << "size = " << data_span.size() << std::endl;
        std::cout << "size bytext = " << data_span.size_bytes() << std::endl;
    }

    /////////////////////////////////////////////////

    void _Tests_() {
        std::array<int, 5> data {1,2,3,4,5};
        std::span<int> data_span(data);

        /*
        std::cout << "sizeof =" << sizeof(data_span) << std::endl;
        std::cout << data_span << std::endl;
        data[2] = 222;
        std::cout << data_span << std::endl;
        data_span[2] = 111;
        std::cout << data_span << std::endl;
        */

        std::span<int, 5> a{data};
        //  std::span<int, 15> a{data}; // ERROR

    }
}

void Span::TEST_ALL()
{
    // Test();

    // Create();
    Create_Not();

    // Front();
    // First();

    // Back();
    // Last();

    // Subspan();

    // Size();

     _Tests_();
}