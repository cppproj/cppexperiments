//============================================================================
// Name        : StructuredBinding.h
// Created on  : 28.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Structured binding declaration tests
//============================================================================

#ifndef STRUCTURED_BINDING_DECLARATION_TESTS__H_
#define STRUCTURED_BINDING_DECLARATION_TESTS__H_

namespace StructuredBinding
{
	void TEST_ALL();
};

#endif /* STRUCTURED_BINDING_DECLARATION_TESTS__H_ */
