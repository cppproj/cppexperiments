	//============================================================================
// Name        : StructuredBinding.cpp
// Created on  : 28.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Structured binding declaration tests
//============================================================================

#include <iostream>
#include <string>
#include <string_view>

#include <cassert>
#include <vector>
#include <map>
#include <array>
#include <set>

#include "StructuredBinding.h"

namespace StructuredBinding {

	struct BookInfo {
		std::string title;
		int yearPublished = 0;
	};

	BookInfo readBookInfo() {
		BookInfo info;
		info.title = "Title";
		info.yearPublished = 2017;
		return info;
	}

	///////////////////////////////////////////////////////

	void VectorTest() {
		std::vector v {std::vector{1, 2}};

		// This is vector<int>, but not vector<vector<int>>
		static_assert(std::is_same_v<std::vector<int>, decltype(v)>);

		// Size is equal 2
		assert(v.size() == 2);

		std::cout << "Done" << std::endl;
	}

	void PairTest() {
		auto pair = std::pair{ 10, "hello" };

		std::cout << pair.first << std::endl;
		std::cout << pair.second << std::endl;
	}

	void Test_Pair() {
		std::pair p = { 1, "hello" };
		auto [first, second] = p;

		std::cout << "Pair value : " << "{" << first << ", " << second << "}" << std::endl;

		std::cout << "\nModifing values." << std::endl;
		first = 0;
		second = "Bie";

		std::cout << "\nPair value : " <<  "{" << first << ", " << second << "}" << std::endl;
	}

	void Test_Array() {
		int coord[3] = { 1, 2, 3 };
		auto[x, y, z] = coord;

		std::cout << "{" << x << ", " << y << ", " << z << "}"  << std::endl;
	}

	void Test_Struct_Reference_Init() {
		struct Config {
			int			id;
			std::string name;
			std::vector<int> data;
		};
		Config cfg;

		auto&[id, n, d] = cfg;

		id = 1;
		n = "name";
		d.push_back(123);

		std::cout << cfg.id << std::endl;
		std::cout << cfg.name << std::endl;
		std::cout << cfg.data[0] << std::endl;
	}

	void Test_Struct() {
		auto[title, year] = readBookInfo();
		std::cout << title << std::endl;
		std::cout << year << std::endl;
	}

	void Test_TryEmplace_Map() {
		std::map<std::string, std::string> map;
		auto[iterator1, succeed1] = map.try_emplace("key", "abc");
		auto[iterator2, succeed2] = map.try_emplace("key", "cde");
		auto[iterator3, succeed3] = map.try_emplace("another_key", "cde");

		assert(succeed1);
		assert(!succeed2);
		assert(succeed3);

		for (auto&&[key, value] : map) {
			std::cout << key << ": " << value << "\n";
		}
	}

	void Test_Insert_Set()
	{
		{
			std::set<int> test_set;
			auto[iter, ok] = test_set.insert(42);
			std::cout << "{" << *iter << ", " << ok << "}" << std::endl;
		}

		std::cout << "\nOld style:\n" << std::endl;

		{
			std::set<int> mySet;
			std::set<int>::iterator iter;
			bool ok;
			std::tie(iter, ok) = mySet.insert(42);
			std::cout << "{" << *iter << ", " << ok << "}" << std::endl;
		}
	}


	void Discard_Param() {
		auto getPair = []() { return std::pair(1, "hello"); };

		auto&& [_, s] = getPair();

		std::cout << _ << std::endl;
	}

	std::array<int, 4> getArray() {
		return {1,2,3,4};
	}

	void Bind_Array() {
		auto[a,b,c,d] = getArray();

		std::cout << a << std::endl;
		std::cout << b << std::endl;
		std::cout << c << std::endl;
		std::cout << d << std::endl;
	}

	struct MyStruct {
		int a;
		std::string str;
	};

	void Move_Test_1() {

		MyStruct ms { 42, "Jim" };
		std::cout << "{" << ms.a << ", " << ms.str << "}\n" << std::endl;


		auto&& [v, n] = std::move(ms);
		std::cout << "{" << v << ", " << n << "}" << std::endl;
		std::cout << "{" << ms.a << ", " << ms.str << "}" << std::endl;
	}

	void Move_Test_2() {

		MyStruct ms{ 42, "Jim" };
		std::cout << "{" << ms.a << ", " << ms.str << "}\n" << std::endl;


		auto [v, n] = std::move(ms);
		std::cout << "{" << v << ", " << n << "}" << std::endl;
		std::cout << "{" << ms.a << ", " << ms.str << "}" << std::endl;
	}

	void TEST_ALL()
	{
		// Test_Struct_Reference_Init()
		// Test_Struct();

		// Test_Pair();
		// Test_Array();
		// Test_TryEmplace_Map();

		// Test_Insert_Set();

		// VectorTest();

		// Discard_Param();

		// Bind_Array();

		// Move_Test_1();
		Move_Test_2();
	}
}