//============================================================================
// Name        : Algorithms.h
// Created on  : 01.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ std algorithms tests
//============================================================================

#ifndef ALGORITMS_INCLUDE_GUARD_H_
#define ALGORITMS_INCLUDE_GUARD_H_

namespace Algorithms {
	void TEST_ALL();
};

#endif /* ANY_CLASS_TESTS_INCLUDE_GUARD_H_ */