//============================================================================
// Name        : ObjectOrientedProgramming.h
// Created on  : 17.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TupleTests testing 
//============================================================================

#ifndef OBJECT_ORIENTED_PROGRAMMING_INCLUDE_GUARD__H
#define OBJECT_ORIENTED_PROGRAMMING_INCLUDE_GUARD__H

namespace ObjectOrientedProgramming {
	void TEST_ALL();
}

#endif // !OBJECT_ORIENTED_PROGRAMMING_INCLUDE_GUARD__H
