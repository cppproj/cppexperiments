//============================================================================
// Name        : TypeTraits.h
// Created on  : 01.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Test support classes and templates
//============================================================================

#ifndef TEST_TRAITS_CLASSES__INCLUDE_GUARD__H
#define TEST_TRAITS_CLASSES__INCLUDE_GUARD__H

namespace TypeTraits {
	void TEST_ALL();
};

#endif // (!TEST_TRAITS_CLASSES__INCLUDE_GUARD__H)