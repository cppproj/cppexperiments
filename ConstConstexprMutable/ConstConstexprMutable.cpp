//============================================================================
// Name        : ConstConstexprMutable.cpp
// Created on  : 09.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Const && Constexpr && Mutable tests
//============================================================================

#include "ConstConstexprMutable.h"

#include <array>
#include <algorithm>
#include <string_view>
#include <stdexcept>
#include <numeric>
#include <vector>
#include <map>
#include <iostream>
#include <string>

namespace ConstConstexprMutable::Compile_Time_IF {

	template <typename T>
	auto GetValue(T t) {
		if constexpr (std::is_pointer<T>::value) {
			return *t;
		}
		else {
			return t;
		}
	}

	void GetValueFromPointer() {
		int value = 123;
		int* prtVal = &value;

		std::cout << GetValue(value) << std::endl;
		std::cout << GetValue(prtVal) << std::endl;
	}

	//----------------------------------------------------------------------//

	template<typename T>
	typename std::enable_if<std::is_pointer<T>::value, std::remove_pointer_t<T>>::type GetValue2(T t) {
		return *t;
	}

	template<typename T>
	typename std::enable_if<!std::is_pointer<T>::value, T>::type GetValue2(T t) {
		return t;
	}

	void GetValueFromPointer2() {
		int value = 123;
		int* prtVal = &value;

		std::cout << GetValue2(value) << std::endl;
		std::cout << GetValue2(prtVal) << std::endl;
	}

	//----------------------------------------------------------------------//

	template<int N>
	constexpr bool is_negative() {
		if constexpr (N >= 0)
			return false;
		else
			return true;
	}

	template<typename T>
	constexpr T transform(T a) {
		return a * 2;
	}

	void IsNegative() {
		constexpr bool negative = is_negative<1>();
		std::cout << std::boolalpha << negative << std::endl;

		constexpr bool negative1 = is_negative<-1>();
		std::cout << std::boolalpha << negative1 << std::endl;

		static_assert(is_negative<-1>(), "OK");
		// static_assert(is_negative<1>(), "NOT NEGATIVE");
	}

	//----------------------------------------------------------------------//

	template <typename T>
	std::string toString(const T& x) {
		if constexpr (std::is_same_v<T, std::string>) {
			return x; // statement invalid, if no conversion to string
		}
		else if constexpr (std::is_arithmetic_v<T>) {
			return std::to_string(x); // statement invalid, if x is not numeric
		}
		else {
			return std::string(x); // statement invalid, if no conversion to string
		}
	}

	void ToStringTest() {
		{
			std::string value = "SomeText";
			std::string result = toString(value);
			std::cout << value << " -> " << result << std::endl;
		}
		{
			int value = 12345;
			std::string result = toString(value);
			std::cout << value << " -> " << result << std::endl;
		}
		{
			char value[] = "Some pain text";
			std::string result = toString(value);
			std::cout << value << " -> " << result << std::endl;
		}
	}

	template<typename T>
	void check_X(const T x)
	{
		if constexpr (auto obj = transform(x); std::is_same_v<decltype(obj), T>) {
			std::cout << "transform(x) yields same type\n";
		}
		else {
			static_assert(false, "transform(x) yields different type");
		}
	}

	void CompileTimeIf_WithInitialization() {
		
		constexpr int x = 123;
		check_X(x);
	}
}

//**************************************************************************//

namespace ConstConstexprMutable::ConstexprFuncs {

	constexpr int _constexpr_sum(int a, int b) {
		return a + b;
	}

	void ConstexprSum() {
		constexpr auto __sq = [](auto x) constexpr -> decltype(x) { return x * x; };
		constexpr auto __sq_bad = [](int x) { return x * x; };

		constexpr int a1 = _constexpr_sum(5, 12);
		std::cout << "a1 = " << a1 << std::endl;

		constexpr int a2 = __sq(5);
		std::cout << "a2 = " << a2 << std::endl;

		// constexpr int s2 = __sq_bad(5);
	}

	//-------------------------------------------------------------------------//

	template <int N>
	struct Factorial {
		static constexpr int value = N * Factorial<N - 1>::value;
	};

	template <>
	struct Factorial<0> {
		static constexpr int value = 1;
	};

	void Factorial_Test() {
		std::cout << Factorial<5>::value << std::endl;
	}

	//-------------------------------------------------------------------------//

	constexpr auto sum( int a, int b) {
		constexpr auto result = std::is_constant_evaluated();
		return result;
	}

	void Check_IF_Constexpr() {
		constexpr auto result = sum(1, 2);
		std::cout << result << std::endl;
	}  
}

//**************************************************************************//

namespace ConstConstexprMutable::MutableTests {

	class ImmutableObject {
	private:
		unsigned int var1;
		mutable unsigned int var2;
		std::string info;

	public:
		void Increment_InConst_Method1() const {
#if 0
			var1++;
#endif
		}

		void Increment_InConst_Method2() const {
			var2++;
		}

		const std::string& getInfo_Const() {
			return this->info;
		}

		std::string& getInfo() {
			return this->info;
		}

	public:
		ImmutableObject() : ImmutableObject(0, 0, "TestInfo") {
		}

		ImmutableObject(unsigned int v1, 
			            unsigned int v2,
					    const std::string& text) : var1(v1), var2(v2), info(text) {
		}

		friend std::ostream& operator<< (std::ostream& stream, ImmutableObject& integer);
	};

	std::ostream& operator<< (std::ostream& stream, ImmutableObject& obj) {
		stream << "[var1 = " << obj.var1 << ", var2 = " << obj.var2 << ", Info: " << obj.info << "]";
		return stream;
	}

	void Update_ImmutableObject() {
		ImmutableObject obj;
		std::cout << obj << std::endl;

		obj.Increment_InConst_Method2();
		std::cout << obj << std::endl;

		obj.getInfo() = "New_Value_1";
		std::cout << obj << std::endl;
#if 0
		obj.getInfo_Const() = "New_Value_1";
		std::cout << obj << std::endl;
#endif
	}
}

//**************************************************************************//

namespace ConstConstexprMutable::Const_Pointers {

	/* Non const teste */
	void NonConstTest() { 
		int int_var = 123;
		int* ptr1 = &int_var;

		std::cout << *ptr1 << std::endl;
		int_var = 222;
		std::cout << *ptr1 << std::endl;
	}

	/* Non const test. */
	void ConstPointerValue() {
		const int  var = 123;
		const int* ptr = &var;

		std::cout << "Value: " <<  *ptr << ". Address: " << ptr << std::endl;
#if 0
		// ERROR - can not change 'var' value
		*ptr1 = 222; 
#endif

		std::cout << "Value: " << *ptr << ". Address: " << ptr << std::endl;
		ptr++; // ITS ALLOWED
		std::cout << "Value: " << *ptr << ". Address: " << ptr << std::endl;
	}

	void ConstPointer() {
		int  var = 123;
		int* const ptr = &var;

		std::cout << "Value: " << *ptr << ". Address: " << ptr << std::endl;
		*ptr = 222; // ITS OK
		std::cout << "Value: " << *ptr << ". Address: " << ptr << std::endl;
#if 0
		// ERROR: you cannot assign to a variable that is const
		// ptr -> is const we cant change it

		// ptr1++;  
#endif

#if 0
		// ERROR: you cannot assign to a variable that is const
		int var2 = 123;
		ptr = &var2;
#endif
	}
}

namespace ConstConstexprMutable::ConstexprMap {
	

	template <typename K, typename V, std::size_t Size>
	class Map {
	public:
		std::array<std::pair<K, V>, Size> data;

		[[nodiscard]]
		constexpr V at(const K &key) const {
			const auto res = std::find_if(data.begin(), data.end(), [&key](const auto &v) {
				return v.first == key;
			});
			if (data.end() != res) {
				return res->second;
			}
			else {
				throw std::range_error("Not Found");
			}
		}
	};

	using namespace std::literals::string_view_literals;
	static constexpr std::array<std::pair<std::string_view, int>, 8> color_values {
		{{"black"sv, 7},
		 {"blue"sv, 3},
		 {"cyan"sv, 5},
		 {"green"sv, 2},
		 {"magenta"sv, 6},
		 {"red"sv, 1},
		 {"white"sv, 8},
		 {"yellow"sv, 4}} };


	int lookup_value(const std::string_view sv) {
		//static const auto map = std::map<std::string_view, int>{color_values.begin(), color_values.end()};
		static constexpr auto map = Map<std::string_view, int, color_values.size()>{ {color_values} };

		return map.at(sv);
	}

	void Test() {
		static constexpr auto testMap = Map<std::string_view, int, color_values.size()>{ {color_values} };
		auto val = testMap.at("green");
		std::cout << val << std::endl;
	}
}


namespace ConstConstexprMutable::ConstexprArray {

	using namespace std::literals::string_view_literals;

	using T = std::pair<int, std::string_view>;
	static constexpr std::array<T, 5> values{ {
		{1, "one"sv},
		{2, "two"sv},
		{3, "three"sv},
		{4, "four"sv},
		{5, "five"sv}
	} };

	consteval std::string_view get(int i) {
		return values[i].second;
	}

	void Constexpr_Test() {
		constexpr auto name = get(3);
		std::cout << name << std::endl;

#if 0
		constexpr auto name = get(values.size() + 1);
#endif
	}
}

namespace ConstConstexprMutable::ConstexprObjects {

	class Point3D {
		const int x;
		const int y;
		const int z;

	public:
		constexpr Point3D(const int x = 0,
					      const int y = 0,
			              const int z = 0) : x{ x }, y{ y }, z{ z } { }

		constexpr int getX() const { 
			return x; 
		}
		constexpr int getY() const { 
			return y; 
		}
		constexpr int getZ() const { 
			return z; 
		}

		friend std::ostream& operator<<(std::ostream& stream, const Point3D& pt);
	};

	std::ostream& operator<<(std::ostream& stream, const Point3D & pt) {
		stream << "[" << pt.x << ", " << pt.y << ", " << pt.z << "]" << std::endl;
		return stream;
	}

	void Test() {
		constexpr Point3D point(10, 10, 10);
		std::cout << point << std::endl;
	}
}

namespace ConstConstexprMutable::Strings {

	using namespace std::string_view_literals;

	void Constexpr_Strings() {
		// constexpr std::string str {"Some_String"};

		constexpr char token[]{"Some_String"};
		constexpr std::string_view str_view = "Some_String"sv;


		std::cout << token << std::endl;
		std::cout << str_view << std::endl;
	}

	/*
	consteval std::string buildString() {
		return "Some_String";
	}
	
	void Constexpr_Strings2() {
		constinit std::string str = buildString();
	}
	*/
}

namespace ConstConstexprMutable::Algoritms {

	void Accumulate() {
		constexpr std::array myArray{ 1, 2, 3, 4, 5 };     
		constexpr auto sum = std::accumulate(myArray.begin(), myArray.end(), 0);  
		std::cout << "sum: " << sum << std::endl;

		constexpr auto product = std::accumulate(myArray.begin(), myArray.end(), 1,      // (3)
			std::multiplies<int>());
		std::cout << "product: " << product << std::endl;

		constexpr auto product2 = std::accumulate(myArray.begin(), myArray.end(), 1,     // (4)
			[](auto a, auto b) { return a * b; });
		std::cout << "product2: " << product2 << std::endl;

		std::cout << std::endl;
	}

}


void ConstConstexprMutable::TEST_ALL() {

	// ConstexprMap::Test();

	 ConstexprArray::Constexpr_Test();
	
	// ConstexprObjects::Test();

	// ConstexprFuncs::ConstexprSum();
	// ConstexprFuncs::Factorial_Test();
	// ConstexprFuncs::Check_IF_Constexpr();

	// Strings::Constexpr_Strings();

	// Algoritms::Accumulate();

	// Compile_Time_IF::GetValueFromPointer();
	// Compile_Time_IF::GetValueFromPointer2();
	// Compile_Time_IF::IsNegative();
	// Compile_Time_IF::ToStringTest();
	// Compile_Time_IF::CompileTimeIf_WithInitialization();

	// MutableTests::Update_ImmutableObject();

	// Const_Pointers::NonConstTest();
	// Const_Pointers::ConstPointer();
	// Const_Pointers::ConstPointerValue();
};
