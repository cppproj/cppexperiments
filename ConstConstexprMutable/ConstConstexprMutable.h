//============================================================================
// Name        : ConstConstexprMutable.h
// Created on  : 09.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Const && Constexpr && Mutable tests
//============================================================================

#ifndef CONST_CONSTEXPR_MUTABLE_TESTS__H_
#define CONST_CONSTEXPR_MUTABLE_TESTS__H_

namespace ConstConstexprMutable {
	void TEST_ALL();
};

#endif /* CONST_CONSTEXPR_MUTABLE_TESTS__H_ */
