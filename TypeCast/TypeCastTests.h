//============================================================================
// Name        : TypeCastTests.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Type casts tests
//============================================================================

#ifndef TYPE_CASTING_TESTS__H_
#define TYPE_CASTING_TESTS__H_

namespace TypeCastTests
{
	void TEST_ALL();
};

#endif /* TYPE_CASTING_TESTS__H_ */
