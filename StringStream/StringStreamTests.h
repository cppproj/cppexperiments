//============================================================================
// Name        : StringstreamTests.h
// Created on  : 28.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Stringstream tests
//============================================================================

#ifndef STRINGSTREAM_TESTS__H_
#define STRINGSTREAM_TESTS__H_

namespace StringStreamTests
{
	void TEST_ALL();
};

#endif /* STRINGSTREAM_TESTS__H_ */
