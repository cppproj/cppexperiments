//============================================================================
// Name        : Memory.h
// Created on  : 14.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Memory tests class 
//============================================================================

#ifndef MEMORY_TESTS_INCLUDE_GUARD__H
#define MEMORY_TESTS_INCLUDE_GUARD__H

#include <iostream>

namespace Memory {
	void TEST_ALL();
};

#endif // !MEMORY_TESTS_INCLUDE_GUARD__H
