//============================================================================
// Name        : InitializerList.h
// Created on  : 18.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Initializer list semantics tests 
//============================================================================

#ifndef INITIALIZER_LIST_INCLUDE_GUARD__H
#define INITIALIZER_LIST_INCLUDE_GUARD__H

#include <iostream>
#include <string>

using String = std::string;
using CString = const String&;

namespace InitializerList
{
	void TEST_ALL();
};

#endif /* INITIALIZER_LIST_INCLUDE_GUARD__H */

