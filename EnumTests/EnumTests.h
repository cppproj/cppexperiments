//============================================================================
// Name        : EnumTests.h
// Created on  : 02.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Enum tests
//============================================================================

#ifndef ENUM_TESTS_INCLUDE_GUARD__H
#define ENUM_TESTS_INCLUDE_GUARD__H

namespace EnumTests {
	void TEST_ALL();
};

#endif // (!ENUM_TESTS_INCLUDE_GUARD__H)
