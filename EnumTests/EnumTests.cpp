//============================================================================
// Name        : EnumTests.h
// Created on  : 02.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Enum tests
//============================================================================

#include "EnumTests.h"

#include <iostream>
#include <string>

namespace EnumTests::EnumNewStyle {

	void EnumsStictType_Test() {
		enum class Options { None, One, All };
		Options opts = Options::All;
	}

	void EnumsStictType_ToInt() {
		enum Options { None, One, All };
		Options opts = Options::All;
		std::cout << opts << std::endl;
	}
};

namespace EnumTests::EnumClassTests {

	enum class Color {
		black,
		white,
		gray
	};

	void __printColor(Color color) {
		switch (color) {
		case Color::gray:
			std::cout << "white + " << std::endl;
			[[fallthrough]];
		case Color::black:
			std::cout << "black" << std::endl;
			break;
		case Color::white:
			std::cout << "white" << std::endl;
			break;
		}
	}

	void PrintColor() {
		std::cout << "Color::black" << std::endl;
		const Color color1 = Color::black;
		__printColor(color1);

		std::cout << "\nColor::white" << std::endl;
		const Color color2 = Color::white;
		__printColor(color2);

		std::cout << "\nColor::gray" << std::endl;
		const Color color3 = Color::gray;
		__printColor(color3);
	}
};

namespace EnumTests::Traits {

	enum class Color {
		Red,
		Green,
		Black
	};

	template <typename T>
	class Traits {
	public:
		static const std::string name(size_t index) {
			return "";
		}
	};

	template <>
	class Traits<Color> {
	public:
		static const std::string name(size_t index) {
			switch (index) {
				case static_cast<int>(Color::Red) :
					return "Red";
				case static_cast<int>(Color::Green) :
					return "Green";
				case static_cast<int>(Color::Black) :
					return "Black";
				default:
					return "unknown";
			}
		}
	};


	void TraitsTests() {
		std::cout << Traits<Color>::name(1) << std::endl;
	}
}

namespace EnumTests::Http {

	enum class Method {
		None = 0,
		GET = 1,
		HEAD = 2,
		POST = 3,
		PUT = 4,
		DELETE = 5,
		CONNECT = 6,
		OPTIONS = 7,
		TRACE = 8,
		PATCH = 9
	};

	std::string toString(Method method) {
		switch (method) {
			case Http::Method::None:
				return "None";
				break;
			case Http::Method::GET:
				return "GET";
				break;
			case Http::Method::HEAD:
				return "HEAD";
				break;
			case Http::Method::POST:
				return "POST";
				break;
			case Http::Method::PUT:
				return "PUT";
				break;
			case Http::Method::DELETE:
				return "DELETE";
				break;
			case Http::Method::CONNECT:
				return "CONNECT";
				break;
			case Http::Method::OPTIONS:
				return "OPTIONS";
				break;
			case Http::Method::TRACE:
				return "TRACE";
				break;
			case Http::Method::PATCH:
				return "PATCH";
				break;
			default:
				return "None";
				break;
		}
	}

	Method fromString(const std::string& method) {
		if (0 == method.compare("GET"))
			return Method::GET;
		else if (0 == method.compare("HEAD"))
			return Method::HEAD;
		else if (0 == method.compare("POST"))
			return Method::POST;
		else if (0 == method.compare("PUT"))
			return Method::PUT;
		else if (0 == method.compare("DELETE"))
			return Method::DELETE;
		else if (0 == method.compare("CONNECT"))
			return Method::CONNECT;
		else if (0 == method.compare("OPTIONS"))
			return Method::OPTIONS;
		else if (0 == method.compare("TRACE"))
			return Method::TRACE;
		else if (0 == method.compare("PATCH"))
			return Method::PATCH;
		return Method::None;
	}

	void Test() {
		Method method1 = fromString("GET");
		std::cout << toString(method1) << std::endl;

		Method method2 = static_cast<Method>(5);
		std::cout << toString(method2) << std::endl;
	}
}

namespace EnumTests::Http2 {

	class HTTPMethod {
	public:
		enum class Method {
			None = 0,
			GET = 1,
			HEAD = 2,
			POST = 3,
			PUT = 4,
			DELETE = 5,
			CONNECT = 6,
			OPTIONS = 7,
			TRACE = 8,
			PATCH = 9
		};

	protected:
		Method method;


	public:
		class HTTPMethod(Method m): method(m) {
		}
	};
}

namespace EnumTests::Iteration {

	enum class Method {
		None = 0,
		GET = 1,
		HEAD = 2,
		POST = 3,
		PUT = 4,
		DELETE = 5,
		CONNECT = 6,
		OPTIONS = 7,
		TRACE = 8,
		PATCH = 9,
		END = 10
	};

	Method& operator ++ (Method& e) {
		if (e == Method::END) {
			throw std::out_of_range("for Method& operator ++ (Method&)");
		}
		e = Method(static_cast<std::underlying_type<Method>::type>(e) + 1);
		return e;
	}

	std::ostream& operator<<(std::ostream& os, Method opt) {
		return os << static_cast<unsigned short>(opt);
	}


	void Test() {
		for (Method m = Method::None; m != Method::END; ++m) {
			std::cout << m << std::endl;
		}
	}
}


namespace EnumTests::DerivedEnums {

	// altitude may be altitude::high or altitude::low
	enum class altitude : char
	{
		high = 'h',
		low = 'l', // C++11 allows the extra comma
	};

	std::ostream& operator<<(std::ostream& os, altitude al) {
		return os << static_cast<char>(al);
	}

	void Test() {

		altitude a = altitude::low;
		std::cout << "a = " << a << std::endl;
		std::cout << sizeof(altitude) << std::endl;
	}

	/********************************************************************************/

	enum class Options : uint32_t {
		None = 0, 
		one = 1 << 0,  
		two = 1 << 1, 
		three = 1 << 2, 
	};

	std::ostream& operator<<(std::ostream& os, Options opt) {
		return os << static_cast<unsigned short>(opt);
	}

	void Bit_Enum_Test() {
		
		std::cout << "Sizeof(Options) = " << sizeof(Options) << "\n" << std::endl;

		std::cout << Options::None << std::endl;
		std::cout << Options::one << std::endl;
		std::cout << Options::two << std::endl;
		std::cout << Options::three << std::endl;
	}
}

namespace EnumTests {
	void TEST_ALL() {
		// EnumClassTests::PrintColor();

		// EnumNewStyle::EnumsStictType_Test();

		// Traits::TraitsTests();

		// Http::Test();

		 DerivedEnums::Test();
		// DerivedEnums::Bit_Enum_Test();

		// Iteration::Test();
	}
};
