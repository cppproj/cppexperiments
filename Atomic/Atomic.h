//============================================================================
// Name        : MoveSemantics_RuleOfFive.h
// Created on  : 1.0
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Move semantics tests (Rule of five)
//============================================================================

#ifndef ATOMIC__INCLUDE_GUARD__H
#define ATOMIC__INCLUDE_GUARD__H

namespace Atomic
{
	void TEST_ALL();
};

#endif //!ATOMIC__INCLUDE_GUARD__H