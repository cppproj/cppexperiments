//============================================================================
// Name        : ReferenceWrapperTests.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Reference Wrapper tests
//============================================================================

#ifndef REFERENCE_WRAPPER_TESTS__H_
#define REFERENCE_WRAPPER_TESTS__H_

namespace ReferenceWrapperTests
{
	void Random_Ints_List();
	void Simple_Tests();
	void CRef_Test();
	void Vector_With_References();
	
	void Vector_List();
	void Vector_Test();

	void Pass_And_Modify_Value();

	void TEST_ALL();
};

#endif /* REFERENCE_WRAPPER_TESTS__H_ */
