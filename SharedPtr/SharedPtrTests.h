//============================================================================
// Name        : SharedPtrTests.h
// Created on  : 21.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Shared PTR C++  tests
//============================================================================

#ifndef SHARED_PTR_TESTS__INCLUDE_GUARD__H
#define SHARED_PTR_TESTS__INCLUDE_GUARD__H

namespace SharedPtr_Tests {
	void Simple_Test_0();
	void Simple_Test_1();
	void Simple_Test_2();
	void Simple_Test_3();
	void Count_Test();
	void Unique_Test();
	void Reset_Test();
	void EnableSharedFromThis_Test_1();
	void BAD_Usage_Test1();
	void VariousTests();
	void CustomDeleter_Test_1();
	void CustomDeleter_Test_2();
	void Swap_Test();
	void Get_Test();
	void Allocate_Shared();
	void MultithreadingTests();

	void TEST_ALL();
};

#endif /* SHARED_PTR_TESTS__INCLUDE_GUARD__H */
