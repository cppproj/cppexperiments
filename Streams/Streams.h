//============================================================================
// Name        : Streams.h
// Created on  : 12.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Streams tests class 
//============================================================================

#ifndef STREAMS_TESTS_INCLUDE_GUARD__H
#define STREAMS_TESTS_INCLUDE_GUARD__H

namespace Streams {
	void TEST_ALL();
};

#endif // !STREAMS_TESTS_INCLUDE_GUARD__H