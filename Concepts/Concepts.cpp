//============================================================================
// Name        : Concepts.h
// Created on  : 04.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Concepts C++20 library tests class 
//============================================================================

#include <iostream>
#include <string>
#include <vector>
#include <string_view>

#include <chrono>
#include <concepts>
#include "Concepts.h"

namespace Concepts {

    class BaseObject {
    public:
    };

    struct Object {
    };

    struct Object_NotMovable {
        Object_NotMovable(Object_NotMovable&& obj) noexcept = delete;
        Object_NotMovable& operator=(Object_NotMovable&& obj) noexcept = delete;
    };

    template<std::movable T>
    class MovableVector {
    private:
        std::vector<T> vector;

    public:
        // MovableVector() {}
        template<class... Args>
        void emplace_back(Args&&... params) {
            vector.emplace_back(std::forward<Args>(params)...);
        }
    };

    void MovableTest() {
        MovableVector<Concepts::Object> v;
        // MovableVector<Concepts::Object_NotMovable> v;
    }

    //--------------------------------------------------------------------------------------------//

    template <class T>
    concept Integral = std::is_integral<T>::value;

    template <class T>
    concept SignedIntegral = Integral<T> && std::is_signed<T>::value;

    template <class T>
    concept UnsignedIntegral = Integral<T> && !SignedIntegral<T>;

    template<SignedIntegral T>
    void handleSignedValue(T val) {    
    }

    template<UnsignedIntegral T>
    void handleUnsignedValue(T val) {
    }


    void Signed_Tests() {
        {
            int a{ 0 };
            handleSignedValue(a);
        }
        {
            unsigned int a{ 0 };

            // ERROR !!!!!!!
            // handleSignedValue(a);
        }
    }

    void Unsigned_Tests() {
        {
            int a{ 0 };

            // ERROR HERE
            // handleUnsignedValue(a);
        }
        {
            unsigned int a{ 0 };
            handleUnsignedValue(a);
        }
    }
};

struct NonHashableClass { };
struct HashableClass { };

namespace std {
    template <>
    struct hash<HashableClass> {
        inline size_t operator()(HashableClass& const x) const {
            return  31;
        }
    };
}

namespace Concepts::Custom_Concepts {

    template<typename T>
    concept Hashable = requires(T a) {
        { std::hash<T>{}(a) } -> std::convertible_to<std::size_t>;
    };

    template<typename Type>
    concept Incrementable = requires(Type var) {
        var++; ++var;
    };

    template<typename Type>
    concept Decreamentable = requires(Type var) {
        var--; 
        --var;
    };

    //--------------------------------------------------------------------------------------//

    template<Hashable T>
    void printHash(T) {  // constrained C++20 function template
        //
    }



    void Hashable_Test() {
        // NonHashableClass a;
        HashableClass a;

        printHash(a);
    }

    //---------------------------------------------------------------------------------//

    template<typename T>
    concept HasSizeAndSwapable = requires(T& a, T& b) {
        { a.swap(b) } noexcept;
        { a.size()  } -> std::convertible_to<std::size_t>;
        // ......
    };

    
    void Has_Size_and_Swappable() {
        // HasSizeAndSwapable - tests???
    }

    //---------------------------------------------------------------------------------//

    template<Incrementable T>
    void Increment(T value) {
        std::cout << "Before: " << value << std::endl;
        value++;
        std::cout << "After: " << value << std::endl;
    }

    template<typename T>
    void Increment2(T value) requires Incrementable<T> {
        std::cout << "Before: " << value << std::endl;
        value++;
        std::cout << "After: " << value << std::endl;
    }

    template<typename T> requires Incrementable<T>
    void Increment3(T value) {
        std::cout << "Before: " << value << std::endl;
        value++;
        std::cout << "After: " << value << std::endl;
    }

    void Incrementable_Test() {
        auto Increment4 = [](Incrementable auto var) {
            std::cout << "Before: " << var << std::endl;
            var++;
            std::cout << "After: " << var << std::endl;
        };



        Increment (1);
        Increment2(1);
        Increment3(1);
        Increment4(1);

#if 0
        Increment(std::string("s"));
        Increment2(std::string("s"))
#endif
    }

    //---------------------------------------------------------------------------------//

    template <class T>
    concept TestObjecttBased = std::is_base_of<BaseObject, T>::value;

    template <class T, class U>
    concept Derived = std::is_base_of_v<U, T>;


    template<TestObjecttBased T>
    class SomeClass {
        T v;
    };

    template<Derived<BaseObject> T>
    class SomeClass1 {
        T v;
    };

    void Custom_Concept_IsBaseOf() {
        [[maybe_unused]]
        SomeClass<BaseObject> a;

        [[maybe_unused]]
        SomeClass1<BaseObject> a1;

#if 0
        // Will not compile since TestObject is not Base of 'std::string':
        SomeClass<std::string> b;
        SomeClass1<std::string> b1;
#endif
        
        std::cout << "OK. Done!" << std::endl;
    }

};


namespace Concepts {
	void TEST_ALL() 
    {
        // MovableTest();
        // Signed_Tests();
       //  Unsigned_Tests();

        // Custom_Concepts::Hashable_Test();
        // Custom_Concepts::Custom_Concept_IsBaseOf();
        // Custom_Concepts::Incrementable_Test();
        Custom_Concepts::Has_Size_and_Swappable();
	}
};
