//============================================================================
// Name        : Concepts.h
// Created on  : 04.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Concepts C++20 library tests class 
//============================================================================

#ifndef CONCEPTS_TESTS_INCLUDE_GUARD__H
#define CONCEPTS_TESTS_INCLUDE_GUARD__H

namespace Concepts {
	void TEST_ALL();
};

#endif // !CONCEPTS_TESTS_INCLUDE_GUARD__H

