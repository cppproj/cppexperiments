//============================================================================
// Name        : FileSystem_Tests.cpp
// Created on  : 17.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : FileSystem testing 
//============================================================================

#include <iostream>
#include <string>
#include <unordered_map>
#include <array>
#include <filesystem>
#include <fstream>
#include <optional>
#include <chrono>
#include <iomanip>

#include "FileSystemTests.h"

namespace FileSystem_Tests {

	using String = std::string;
	using CString = const String&;

	void DirectoryIteratorTest() {
		const std::string dirPath = R"(S:\Temp\Folder_For_Testing)";
		for (const auto& path : std::filesystem::directory_iterator(dirPath)) {
			std::cout << path.path() << std::endl;
		}
	}

	void DirectoryIteratorTest_Recursive() {
		const std::string dirPath = R"(S:\Temp\Folder_For_Testing)";
		for (const auto& path : std::filesystem::recursive_directory_iterator(dirPath)) {
			std::cout << path.path() << std::endl;
		}
	}

	void Enumerate_Directory() {
		const std::filesystem::path dir = R"(S:\Temp\Folder_For_Testing)";
		if (std::filesystem::exists(dir) && std::filesystem::is_directory(dir)) {
			for (auto const & entry : std::filesystem::recursive_directory_iterator(dir)) {
				auto filename = entry.path().filename();
				if (std::filesystem::is_directory(entry.status()))
					std::cout << "[+]" << filename << std::endl;
				else if (std::filesystem::is_symlink(entry.status()))
					std::cout << "[>]" << filename << std::endl;
				else if (std::filesystem::is_regular_file(entry.status()))
					std::cout << " " << filename << std::endl;
				else
					std::cout << "[?]" << filename << std::endl;
			}
		}
	}

	std::uintmax_t dir_size(const std::filesystem::path& path)
	{
		std::uintmax_t size = 0;
		if (std::filesystem::exists(path) && std::filesystem::is_directory(path))
		{
			for (const auto& entry : std::filesystem::recursive_directory_iterator(path)) {
				if (std::filesystem::is_regular_file(entry.status()) || std::filesystem::is_symlink(entry.status()))
				{
					auto err = std::error_code{};
					auto filesize = std::filesystem::file_size(entry);
					if (filesize != static_cast<uintmax_t>(-1))
						size += filesize;
				}
			}
		}
		return size;
	}

	void Directory_Size() {
		const std::filesystem::path dir = R"(S:\Temp\Folder_For_Testing)";
		std::cout << "Size = " << dir_size(dir) << std::endl;
	}

	void CreateDirectoryTest() {
		namespace fs = std::filesystem;

		const String dir1 = "C:\\Temp\\TEST_DIRS\\DirToCreate1";
		bool result = fs::create_directory(dir1);
		std::cout << "Create directory result : " << result << std::endl;

		// Check is dir exists:
		if (fs::exists(dir1)) {
			std::cout << "Directory '" << dir1 << "' exists" << std::endl;
		}

		const String dirs = "C:\\Temp\\TEST_DIRS\\DirToCreate1\\DirToCreate2\\DirToCreate3";
		result = fs::create_directories(dirs);
		std::cout << "Create directories result : " << result << std::endl;

		// Check is dir exists:
		if (fs::exists(dir1)) {
			std::cout << "Directories '" << dirs << "' exists" << std::endl;
		}
	}

	void DeleteDirectoryTest() {
		namespace fs = std::filesystem;

		const String dir1 = "C:\\Temp\\TEST_DIRS\\DirToCreate1";
		const String dirs = "C:\\Temp\\TEST_DIRS\\DirToCreate1\\DirToCreate2\\DirToCreate3";

		try {
			bool result = fs::remove(dir1);
			std::cout << "Remove directory '" << dir1 << "' result : " << result << std::endl;
		}
		catch (const std::exception& exc) {
			std::cout << "Failed to delete dir '" << dir1 << "'." << std::endl;
			std::cout << exc.what() << std::endl;
		}

		try {
			bool result = fs::remove_all(dir1);
			std::cout << "RemoveAll directories '" << dir1 << "' result : " << result << std::endl;
		}
		catch (const std::exception& exc) {
			std::cout << "Failed to delete dir '" << dir1 << "'." << std::endl;
			std::cout << exc.what() << std::endl;
		}
	}

	void TempDirectoryPath() {
		namespace fs = std::filesystem;
		fs::path tempDir = fs::temp_directory_path();
		std::cout << "Temporary directory path : " << tempDir << std::endl;
	}

	void CopyDirTest() {
		namespace fs = std::filesystem;

		const String srcDir = "C:\\Temp\\TEST_DIRS\\html_dir";
		const String dstDir = "C:\\Temp\\TEST_DIRS\\html_dir_copy";

		std::error_code code;

		fs::copy(srcDir, dstDir, code);
		if (0 == code.value())
			std::cout << "'" << srcDir << "' copied to '" << dstDir << "' OK." << std::endl;

		std::cout << "\nAttempt 2...\n" << std::endl;

		fs::copy(srcDir, dstDir, code);
		if (0 != code.value())
			std::cout << "Failed to copy dir'" << srcDir << "' to '" << dstDir << "'. Error code = " << code.value() << std::endl;

	}

	void StandartMethods2() {

		namespace fs = std::filesystem;
		using namespace std::chrono_literals;

		const std::string filePath = "C:\\Temp\\FILES\\TestFile.txt";
		fs::path file(filePath);

		std::cout << "Root path (file) : " << file.root_path() << std::endl;
		std::cout << "Root name (file) : " << file.root_name() << std::endl;
		std::cout << "Relative Path (file) : " << file.relative_path() << std::endl;
		std::cout << "File name (file) : " << file.filename() << std::endl;
		std::cout << "Full path (file) : " << file.root_path().string() + file.relative_path().string() << std::endl;

		std::cout << "--------------------------------------------------------------------" << std::endl;

		fs::path currentPath = fs::current_path();

		std::cout << "Root path (current path) : " << currentPath.root_path() << std::endl;
		std::cout << "Root name (current path) : " << currentPath.root_name() << std::endl;
		std::cout << "Relative Path (current path) : " << currentPath.relative_path() << std::endl;
		std::cout << "File name (current path) : " << currentPath.filename() << std::endl;

		/*
		std::ofstream(p).put('a'); // create file of size 1
		std::cout << "File size = " << fs::file_size(p) << '\n';
		fs::remove(p);
		try {
			fs::file_size("/dev"); // attempt to get size of a directory
		}
		catch (fs::filesystem_error& e) {
			std::cout << e.what() << '\n';
		}*/

	}

	void CreateDirectory()
	{
		std::filesystem::create_directories("R:\\Temp\\FILES\\TestDirs\\a\\b");
		std::vector<std::filesystem::path> paths = {
			"R:\\Temp\\FILES\\TestDirs\\a\\b\\1.txt",
			"R:\\Temp\\FILES\\TestDirs\\a\\b\\2.txt",
			"R:\\Temp\\FILES\\TestDirs\\a\\3.dat",
			"R:\\Temp\\FILES\\TestDirs\\4.dat"
		};
		for (const auto& filepath : paths) {
			std::ofstream(filepath.native()) << "test";
		}

		{
			std::filesystem::recursive_directory_iterator begin("R:\\Temp\\FILES\\TestDirs");
			std::filesystem::recursive_directory_iterator end;
			std::vector<std::filesystem::path> subdirs;
			std::copy_if(begin, end, std::back_inserter(subdirs), [](const std::filesystem::path& path) {
				return std::filesystem::is_directory(path);
			});

			std::cout << "-- subdirs --" << std::endl;
			std::copy(subdirs.begin(), subdirs.end(), std::ostream_iterator<std::filesystem::path>(std::cout, "\n"));
		}


		{
			std::filesystem::recursive_directory_iterator begin("R:\\Temp\\FILES\\TestDirs");
			std::filesystem::recursive_directory_iterator end;

			// Get file list of extension .txt using algirithm copy_if
			std::vector<std::filesystem::path> txtFiles;
			std::copy_if(begin, end, std::back_inserter(txtFiles), [](const std::filesystem::path& path) {
				return std::filesystem::is_regular_file(path) && (path.extension() == ".txt");
			});
			// List files .
			std::cout << "-- txt files --" << std::endl;
			std::copy(txtFiles.begin(), txtFiles.end(), std::ostream_iterator<std::filesystem::path>(std::cout, "\n"));
		}
	}

	void Is_Dir_Exists() {
		const std::filesystem::path dirPath(R"(S:\Temp\Folder_For_Testing\File_1.txt)");

		// Check is dir exists:
		if (std::filesystem::exists(dirPath)) {
			std::cout << "Directory " << dirPath << " exists" << std::endl;
		}
	}

	void Is_Dir_Exists_ErrrCpde() {
		const std::filesystem::path dirPath(R"(S:\Temp\Folder_For_Testing\File_1.txt)");

		std::error_code errCode;
		if (std::filesystem::exists(dirPath, errCode)) {
			std::cout << "Directory " << dirPath << " exists" << std::endl;
		}

		std::cout << "errCode.value() = " << errCode.value() << std::endl;
		std::cout << "errCode.message() = " << errCode.message() << std::endl;
		// std::cout << "errCode.message() = " << errCode.<< std::endl;
	}

	void CurrentPath() {
		std::filesystem::path currentPath = std::filesystem::current_path();
		std::cout << currentPath << std::endl;
	}

	void Hard_Link_Count() {
		std::filesystem::path path = std::filesystem::current_path();
		std::cout << path << std::endl;
		
		std::cout << "Number of hard links for current path is " 
			      << std::filesystem::hard_link_count(path) << std::endl;

		// each ".." is a hard link to the parent directory, so the total number
		// of hard links for any directory is 2 plus number of direct subdirectories
		path = std::filesystem::current_path() / ".."; // each dot-dot is a hard link to parent
		std::cout << path << std::endl;

		std::cout << "Number of hard links for .. is " << std::filesystem::hard_link_count(path) << std::endl;
	}
};

namespace FileSystem_Tests::DocumentStorageTests {

	class DocumentStorage {
	private:
		/** WebApps directory files in-memory storage: **/
		std::unordered_map<std::string, std::string> documents;

		/** WebApps director full path. **/
		std::string webAppsDir;

	public:
		DocumentStorage(const std::string& path = "") {
			webAppsDir = path;
		}

	public:
		std::optional<std::unordered_map<std::string, std::string>::const_iterator>
		GetResource(const std::string& path) const noexcept {
			std::optional<std::unordered_map<std::string, std::string>::const_iterator> result;
			if (auto iter = documents.find(path); documents.end() != iter)
				result.emplace(iter);
			return result;
		}

		/** **/
		bool ReadWebAppDir() {
			for (const auto& path : std::filesystem::recursive_directory_iterator(this->webAppsDir))
				if (true == path.is_regular_file() && false == ReadFile(path.path()))
					return false;
			return false;
		}

	protected:
		/** **/
		bool ReadFile(const std::filesystem::path& path) {
			try {
				std::string relative_path(path.string().substr(this->webAppsDir.size(), path.string().length() - this->webAppsDir.size()));
				auto inserted = this->documents.emplace(relative_path, "");

				// std::cout << "Readinf file " << relative_path << std::endl;

				/**/
				std::ifstream input_file_stream(path);
				inserted.first->second.reserve(std::filesystem::file_size(path));

				input_file_stream.unsetf(std::ios::skipws);
				std::copy(std::istream_iterator<char>(input_file_stream),
						  std::istream_iterator<char>(),
						  std::back_inserter(inserted.first->second));
				input_file_stream.close();
			}
			catch (const std::exception& exc)
			{
				std::cout << "ERROR:" << exc.what() << std::endl;
			}
			return true;
		}
	};

	/////////////////////////////////

	void Test() {
		DocumentStorage documentStorage("R:\\Projects\\Html");
		documentStorage.ReadWebAppDir();
	}
}


namespace FileSystem_Tests::Files {
	namespace fs = std::filesystem;

	/* Test directory: */
	static const std::string testDir = R"(S:\Temp\Folder_For_Testing)";

	void CrateFile() {
		auto testFile = std::filesystem::path(testDir + R"(\Read_Write_Files\File_to_Create.txt)");
		std::ofstream dataFile(testFile);
		if (!dataFile) {
			std::cerr << "OOPS, can't open \"" << testFile.string() << "\"\n";
			std::exit(EXIT_FAILURE); // exit program with failure
		}

		// Write to file:
		dataFile << "Hello!";
	}

	void CopyFile() {
		auto src = fs::path(testDir + R"(\COPY\Input.txt)");
		auto dst = fs::path(testDir + R"(\COPY\Output1.txt)");

		try {
			fs::copy_file(src, dst);
			std::cout << dst << ". Created: " << std::boolalpha << fs::exists(dst) << std::endl;
		}
		catch (std::exception& exc) {
			std::cout << exc.what() << std::endl;
		}
	}


	void Last_Write_Time() {
		using namespace std::chrono_literals;
		const auto file = fs::path(testDir + R"(\File_1.txt)");

		auto lwt = std::filesystem::last_write_time(file);

		//std::time_t cftime = decltype(lwt)::clock::
		//std::cout << "File write time is " << std::asctime(std::localtime(&cftime)) << '\n';
	}

	void File_Params() {
		const auto file = fs::path(testDir + R"(\File_1.txt)");

		std::cout << "exists: " << std::filesystem::exists(file) << "\n"
			<< "root_name: " << file.root_name() << "\n"
			<< "root_path: " << file.root_path() << "\n"
			<< "relative_path: " << file.relative_path() << "\n"
			<< "parent_path: " << file.parent_path() << "\n"
			<< "filename: " << file.filename() << "\n"
			<< "stem: " << file.stem() << "\n"
			<< "extension: " << file.extension() << "\n\n";

		std::cout << "has_extension: " << std::boolalpha << file.has_extension() << "\n"
			<< "has_filename: " << std::boolalpha << file.has_filename() << "\n"
			<< "has_parent_path: " << std::boolalpha << file.has_parent_path() << "\n"
			<< "has_relative_path: " << std::boolalpha << file.has_relative_path() << "\n"
			<< "has_root_directory: " << std::boolalpha << file.has_root_directory() << "\n"
			<< "has_root_name: " << std::boolalpha << file.has_root_name() << "\n"
			<< "has_root_path: " << std::boolalpha << file.has_root_path() << "\n"
			<< "has_stem: " << std::boolalpha << file.has_stem() << "\n";
	}
}




namespace FileSystem_Tests::Permissions {


	void demo_perms(std::filesystem::perms p)
	{
		std::cout << ((p & std::filesystem::perms::owner_read) != std::filesystem::perms::none ? "r" : "-")
			<< ((p & std::filesystem::perms::owner_write) != std::filesystem::perms::none ? "w" : "-")
			<< ((p & std::filesystem::perms::owner_exec) != std::filesystem::perms::none ? "x" : "-")
			<< ((p & std::filesystem::perms::group_read) != std::filesystem::perms::none ? "r" : "-")
			<< ((p & std::filesystem::perms::group_write) != std::filesystem::perms::none ? "w" : "-")
			<< ((p & std::filesystem::perms::group_exec) != std::filesystem::perms::none ? "x" : "-")
			<< ((p & std::filesystem::perms::others_read) != std::filesystem::perms::none ? "r" : "-")
			<< ((p & std::filesystem::perms::others_write) != std::filesystem::perms::none ? "w" : "-")
			<< ((p & std::filesystem::perms::others_exec) != std::filesystem::perms::none ? "x" : "-")
			<< '\n';
	}

	void Get_File_Permissions() {
		const std::filesystem::path file = R"(S:\Temp\Folder_For_Testing\Read_Write_Files\read_write.txt)";
		demo_perms(std::filesystem::status(file).permissions());
	}

	void If_File_Writable() {
		{
			const std::filesystem::path file = R"(S:\Temp\Folder_For_Testing\Read_Write_Files\read_write.txt)";
			auto status = std::filesystem::status(file);

			std::cout << "File: " << file << std::endl;
			if ((status.permissions() & std::filesystem::perms{ 0222 }) != std::filesystem::perms::none) {
				std::cout << "File writable" << std::endl;
			}
		}

		{
			const std::filesystem::path file = R"(S:\Temp\Folder_For_Testing\Read_Write_Files\no_writable_file.txt)";
			auto status = std::filesystem::status(file);

			std::cout << "File: " << file << std::endl;
			if ((status.permissions() & std::filesystem::perms{ 0222 }) != std::filesystem::perms::none) {
				std::cout << "File writable" << std::endl;
			}
			else 
				std::cout << "File NON writable" << std::endl;
		}

	}
}

void FileSystem_Tests::TEST_ALL() {
	// TempDirectoryPath();
	// StandartMethods();
	// StandartMethods2();

	// DirectoryIteratorTest();
	// DirectoryIteratorTest_Recursive();
	// Enumerate_Directory();

	// CreateDirectory();
	// CreateDirectoryTest();
	// DeleteDirectoryTest();
	// CopyDirTest();

	// Is_Dir_Exists();
	// Is_Dir_Exists_ErrrCpde();

	// Directory_Size();

	// CurrentPath();

	// Hard_Link_Count();

	// Files::File_Params();
	// Files::CrateFile();
	// Files::CopyFile();
	// Files::Last_Write_Time();


	// Permissions::Get_File_Permissions();
	// Permissions::If_File_Writable();

	 DocumentStorageTests::Test();

}