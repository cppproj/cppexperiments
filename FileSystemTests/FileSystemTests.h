//============================================================================
// Name        : FileSystem_Tests.h
// Created on  : 17.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : FileSystem testing 
//============================================================================

#ifndef FILESYSTEM_TESTING_INCLUDE_GUARD__H
#define FILESYSTEM_TESTING_INCLUDE_GUARD__H

namespace FileSystem_Tests {
	void TEST_ALL();
}

#endif // !FILESYSTEM_TESTING_INCLUDE_GUARD__H