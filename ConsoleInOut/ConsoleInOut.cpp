//============================================================================
// Name        : ConsoleInOut.cpp
// Created on  : 28.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Stringstream tests
//============================================================================

#define _CRT_SECURE_NO_WARNINGS

#include <cinttypes>
#include <iostream>
#include <string>
#include <sstream>
#include <string_view>
#include <vector>
#include <iomanip>
#include <locale>

#include "ConsoleInOut.h"

namespace Color {
	enum Code {
		FG_RED = 31,
		FG_GREEN = 32,
		FG_BLUE = 34,
		FG_DEFAULT = 39,
		BG_RED = 41,
		BG_GREEN = 42,
		BG_BLUE = 44,
		BG_DEFAULT = 49
	};

	class Modifier {
		Code code;
	public:
		Modifier(Code pCode) : code(pCode) {}
		friend std::ostream&
			operator<<(std::ostream& os, const Modifier& mod) {
			return os << "\033[" << mod.code << "m";
		}
	};
}


namespace ConsoleInOut
{
	void Simple_Read()
	{
		std::string input;
		std::cout << "Enter text: ";
		std::getline(std::cin, input);
		std::cout << "Text: " << input << std::endl;
	}

	void Simple_Read_2()
	{
		std::string a, b;
		std::cin >> a >> b;
		std::cout << a << std::endl;
		std::cout << b << std::endl;
	}

	void ReadLines()
	{
		std::string line;
		std::getline(std::cin, line);
		//std::cout << "Line: " << line << std::endl;
		std::getline(std::cin, line);
		std::cout << "Line: " << line << std::endl;
	}

	void Read_4_Str() {
		std::string str;
		std::vector<std::string> vec;

		std::cout << "Enter 4 sentense:";
		while ((std::getline(std::cin, str, ';')) && (vec.size() < 4))
		{
			std::cout << "str   " << str << std::endl;
			vec.push_back(str);
		}
	}

	void Read_And_Convert_To_Ints()
	{
		std::string mystr;
		float price = 0;
		int quantity = 0;

		std::cout << "Enter price: ";
		std::getline(std::cin, mystr);
		std::stringstream(mystr) >> price;

		std::cout << "Enter quantity: ";
		std::getline(std::cin, mystr);
		std::stringstream(mystr) >> quantity;

		std::cout << "Total price: " << price * quantity << std::endl;
	}

	void Gcount_Test()
	{
		char str[20];
		std::cout << "Please, enter a word: ";
		std::cin.getline(str, 20);
		std::cout << std::cin.gcount() << " characters read: " << str << '\n';
	}

	void Scanf_Test() {

		[[maybe_unused]] int n;
		std::int64_t lld = 0;
		[[maybe_unused]] char c;
		[[maybe_unused]] float f;
		[[maybe_unused]] double l5;

		/*
		scanf("%d %lld %c %f %lf", &n, &lld, &c, &f, &lf);

		std::cout << n << std::endl;
		std::cout << lld << std::endl;
		std::cout << c << std::endl;
		printf("%3.3f\n", f);
		printf("%5.9f\n", lf);
		*/

		scanf("%lf", &l5);
		printf("%5.9lf\n", l5);
	}

	void OutputFormat() {
		std::cout << std::setw(5) << 0.2 << std::setw(10) << 123456 << std::endl;
		std::cout << std::setw(5) << 0.12 << std::setw(10) << 123456789 << std::endl;
	}

	void OutputFormat2()
	{
		//std::cout.imbue(std::locale("en_US.utf8"));

		std::cout << "Left fill:\n" << std::left << std::setfill('*')
			<< std::setw(12) << -1.23 << '\n'
			<< std::setw(12) << std::hex << std::showbase << 42 << std::endl;
			//<< std::setw(12) << std::put_money(123, true) << "\n\n";

		/*

			std::cout << "Internal fill:\n" << std::internal
			<< std::setw(12) << -1.23 << '\n'
			<< std::setw(12) << 42 <<  std::endl;
			//<< std::setw(12) << std::put_money(123, true) << "\n\n";

			std::cout << "Right fill:\n" << std::right
			<< std::setw(12) << -1.23 << '\n'
			<< std::setw(12) << 42 << std::endl;
			//<< std::setw(12) << std::put_money(123, true) << '\n';
			*/
	}

	void Fill() {

		std::cout << std::left << std::setfill('*') << std::setw(20) << "TEXT" << std::endl;
		std::cout << std::right << std::setfill('*') << std::setw(20) << "TEXT" << std::endl;
		std::cout << std::internal << std::setfill('*') << std::setw(20) << "TEXT" << std::endl;
	}

	void PrintHex() {
		std::cout << std::setw(12) << std::hex << std::showbase << 42 << std::endl;
		std::cout << std::setw(12) << std::hex << std::showbase << 100 << std::endl;
	}

	void SetPrecision() {
		double f = 3.14159;
		std::cout << std::setprecision(2) << f << std::endl;
		std::cout << std::setprecision(3) << f << std::endl;
		std::cout << std::setprecision(5) << f << std::endl;
		std::cout << std::setprecision(9) << f << std::endl;
		std::cout << std::fixed;
		std::cout << std::setprecision(5) << f << std::endl;
		std::cout << std::setprecision(9) << f << std::endl;
	}

	void Showpos() {
		int a = 10;
		int b = -120;

		std::cout << std::showpos << a << std::endl;
		std::cout << std::showpos << b << std::endl;
	}

	void TEST() {
		double A = 100.345;
		double B = 2006.008;
		double C = 2331.41592653498;

		std::cout << std::hex << std::showbase << (int)A << std::endl;
		std::cout << std::fixed << std::setw(15) << std::setprecision(2) << std::setfill('_') << std::right << std::showpos << B << std::endl;

		// LINE 3
		std::cout << std::scientific << std::uppercase << std::noshowpos << std::setprecision(9); // formatting
		std::cout << C << std::endl; // actual printed part
	}
}

/** TEST **/
namespace ConsoleInOut
{
	void TEST_ALL() {
		// Simple_Read();
		// Simple_Read_2();
		// ReadLines();
		// Read_4_Str();
		// Read_And_Convert_To_Ints();
		// Gcount_Test();
		// Scanf_Test();
		
		// Fill();
		// PrintHex();
		// SetPrecision();

		// OutputFormat();
		// OutputFormat2();

		// Showpos();

		// TEST();
	}
};

