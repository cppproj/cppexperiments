//============================================================================
// Name        : ConsoleInOut.h
// Created on  : 28.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Stringstream tests
//============================================================================

#ifndef CONSOLE_IN_OUT_TESTS__H_
#define CONSOLE_IN_OUT_TESTS__H_

namespace ConsoleInOut
{
	void TEST_ALL();
};

#endif /* CONSOLE_IN_OUT_TESTS__H_ */
