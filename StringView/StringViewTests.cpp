//============================================================================
// Name        : StringViewTests.cpp
// Created on  : 24.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : StringViewTests C++  tests
//============================================================================

#include <iostream>
#include <string>
#include <string_view>
#include <chrono>
#include <charconv>
#include <array>

#include "../Integer/Integer.h"
#include "StringViewTests.h"

namespace StringView {
	std::string_view askForName()
	{
		std::cout << "What's your name?\n";

		std::string str{};
		std::cin >> str;

		std::string_view view{ str };

		std::cout << "Hello " << view << '\n';

		return view;
	} // str get destroy
};

namespace StringView {

	class Constants {
	public:
		constexpr static std::string_view getName() {
			return "Some_Constat_Name";
		}
	};

	void Test() {
		std::string str = "lllloooonnnngggg sssstttrrriiinnnggg"; //A really long string

		//Bad way - 'string::substr' returns a new string (expensive if the string is long)
		std::cout << "std::string value: " << str.substr(0, 16) << std::endl;

		//Good way - No copies are created!
		std::string_view view = str;

		// string_view::substr returns a new string_view
		std::cout << "std::string_view value: " << view.substr(0, 16) << std::endl;
	}

	void Create() {
		{
			char chars[] = { 'a', 'e', 'i', 'o', 'u' };
			std::string_view str { chars, std::size(chars) };

			std::cout << str << std::endl;
		}
	}

	void ToString() {
		const auto print_str = [](const std::string& text)-> void {
			std::cout << text << std::endl;
		};

		std::string_view sv{ "balloon" };
		sv.remove_suffix(3);
		std::string str{ sv };
		print_str(str);
		print_str(static_cast<std::string>(sv));
	}

	void ReturnSrting()
	{
		const auto get_str_viw = []()-> std::string_view {
			std::string str = "ETETETET";
			return std::string_view(str);
		};

		std::cout  << get_str_viw() << std::endl;
	}

	void Create_2() {
		std::string_view view{ askForName() };
		std::cout << "Your name is " << view << std::endl;
	}

	void Basic_Tests()
	{
		std::string_view stringView{ "Trains are fast!" };

		// Length.
		// Result: 16
		std::cout << stringView.length() << std::endl;

		// Result: Trains
		std::cout << stringView.substr(0, stringView.find(' ')) << std::endl;

		// Result: 1
		std::cout << (stringView == "Trains are fast!") << std::endl;

		// Since C++20
		// std::cout << str.starts_with("Boats") << '\n'; // 0
		// std::cout << str.ends_with("fast!") << '\n'; // 1

		// Result: Trains are fast!
		std::cout << stringView << std::endl;

		std::cout << "empty: " << stringView.empty() << std::endl;

		stringView.remove_prefix(7);
		std::cout << "'remove_prefix(7)' result: " << stringView << ".   Length: " << stringView.length() << std::endl;

		stringView.remove_suffix(5);
		std::cout << "'remove_suffix(5)' result: " << stringView << ".   Length: " << stringView.length() << std::endl;

	}

	void  Basic_Tests_2() {

		char arr[] { "Gold" };
		std::string_view str{ arr };

		std::cout << str << std::endl;
		arr[3] = 'f';
		std::cout << str << std::endl;
	}

	void worker_str(const std::string& str) {
		int len = str.length();
		//std::cout << len << std::endl;
	}

	void  worker_str_view(std::string_view str) {
		int len = str.length();
		//std::cout << len << std::endl;
	}

	void PerformanceTest() {
		std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

		constexpr char text[] = "Some_test_value";
		//std::this_thread::sleep_for(std::chrono::milliseconds(125));

		for (int i = 0; i < 10000; i++) {
			for (int n = 0; n < 10000; n++) {
				// StringView_Tests::worker_str(text);
				StringView::worker_str_view(text);
			}
		}

		std::chrono::steady_clock::time_point stop = std::chrono::steady_clock::now();
		double duration = std::chrono::duration_cast<std::chrono::duration<double>>(stop - start).count();
		std::cout << "Excution time: " << duration << std::endl;
	}

	template<class Type>
	Type atoi_17(std::string_view str) {
		Type res{};
		std::from_chars(str.data(), str.data() + str.size(), res);
		return res;
	}


	void GlobalConstVar() {
		std::cout << Constants::getName() << std::endl;
	}

	void Various_Tests() {
		// std::literals::string_view_literals::operator""sv;
		using namespace std::literals;

		std::string_view s1 = "abc\0\0def";
		std::string_view s2 = "abc\0\0def"sv;
	}
};

void StringView::TEST_ALL() {
	// Create();
	// Create_2();
	// Test();
	// Basic_Tests();
	// Basic_Tests_2();
	// ToString();
	// ReturnSrting();

	// Various_Tests();

	GlobalConstVar();
}