//============================================================================
// Name        : StringViewTests.h
// Created on  : 24.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : StringViewTests C++  tests
//============================================================================

#ifndef STRING_VIEW_TESTS__INCLUDE_GUARD__H
#define STRING_VIEW_TESTS__INCLUDE_GUARD__H

namespace StringView {
	void TEST_ALL();
};

#endif /* STRING_VIEW_TESTS__INCLUDE_GUARD__H */

