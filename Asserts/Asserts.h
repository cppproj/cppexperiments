//============================================================================
// Name        : Asserts.h
// Created on  : 06.07.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ std asserts tests
//============================================================================

#ifndef ASSERTS_TESTS_INCLUDE_GUARD_H_
#define ASSERTS_TESTS_INCLUDE_GUARD_H_

namespace Asserts {
	void TEST_ALL();
};

#endif /* ASSERTS_TESTS_INCLUDE_GUARD_H_ */

