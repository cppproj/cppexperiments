//============================================================================
// Name        : Locale.h
// Created on  : 15.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Locale  tests
//============================================================================

#include <iostream>
#include <locale>
#include "Locale.h"

namespace Locale
{
	void IsSpace()
	{
		int i = 0;
		char str[] = "Example sentence to test isspace\n", c;
		while (str[i]) {
			c = str[i++];
			if (isspace(c)) 
				c = '\n';
			std::cout << c;
		}
	}

	void IsCntrl()
	{
		int i = 0;
		char str[] = "first line \n second line \n";
		while (!iscntrl(str[i]))
		{
			std::cout << str[i];
			i++;
		}
	}

	void IsUpper_ToLower()
	{
		int i = 0;
		std::string str = "TeSt-TeSt";
		std::cout << str << "  --->  ";
		for (auto& c : str) {
			if (isupper(c))
				c = tolower(c);
		}
		std::cout << str << std::endl;
	}

	void IsLower_ToUpper()
	{
		int i = 0;
		std::string str = "test-test";
		std::cout << str << "  --->  ";
		for (auto& c: str) {
			if (islower(c))
				c = toupper(c);
		}
		std::cout << str << std::endl;
	}
};

namespace Locale
{
	void TEST_ALL() {
		// IsSpace();
		// IsCntrl();
		 IsUpper_ToLower();
		// IsLower_ToUpper();
	}
};