//============================================================================
// Name        : Locale.h
// Created on  : 15.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Locale  tests
//============================================================================

#ifndef LOCALE_TESTS__INCLUDE_GUARD__H
#define LOCALE_TESTS__INCLUDE_GUARD__H

namespace Locale
{
	void TEST_ALL();
};

#endif /* LOCALE_TESTS__INCLUDE_GUARD__H */
