//============================================================================
// Name        : Literals.h
// Created on  : 24.09.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Literals tests
//============================================================================

#ifndef LITERALS_TESTS_INCLUDE_GUARD_H_
#define LITERALS_TESTS_INCLUDE_GUARD_H_

namespace Literals {
	void TEST_ALL();
};

#endif /* LITERALS_TESTS_INCLUDE_GUARD_H_ */
