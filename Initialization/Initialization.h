//============================================================================
// Name        : Initialization.h
// Created on  : 09.11.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Initialization tests 
//============================================================================

#ifndef INITIALIZATION_INCLUDE_GUARD__H
#define INITIALIZATION_INCLUDE_GUARD__H

namespace Initialization {
	void TEST_ALL();
};

#endif /* INITIALIZATION_INCLUDE_GUARD__H */

