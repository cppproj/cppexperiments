//============================================================================
// Name        : TemplatesTests.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Templates tests
//============================================================================

#ifndef TEMPLATES_TESTS__H_
#define TEMPLATES_TESTS__H_

namespace Templates {

	void Rank();

	void Is_Base_Of__Test();

	void Is_Same();

	void Enable_If_Test();

	void Enable_If_Test2();

	void Enable_If_CheckPointer();

	void TEST_ALL();

};

#endif /* TEMPLATES_TESTS__H_ */
