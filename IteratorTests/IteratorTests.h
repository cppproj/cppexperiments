//============================================================================
// Name        : IteratorTests.h
// Created on  : 20.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Iterator C++ libraty tests
//============================================================================

#ifndef ITERATOR_TESTS__INCLUDE_GUARD__H
#define ITERATOR_TESTS__INCLUDE_GUARD__H

#include <iostream>
#include <string>
#include <string_view>

namespace IteratorTests {

	void FrotInserver();
	void Front_Insert_Iterator();

	void Move_Iterator();

	void Back_Insert();
	void Back_Insert_Iterator();

	void Front_Insert_Iterator();

	void Advance();

	void TEST_ALL();
};

#endif /* ITERATOR_TESTS__INCLUDE_GUARD__H */
