//============================================================================
// Name        : WeakPtrTests.h
// Created on  : 23.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Weak PTR C++  tests
//============================================================================

#ifndef WEAK_PTR_TESTS__INCLUDE_GUARD__H
#define WEAK_PTR_TESTS__INCLUDE_GUARD__H

namespace WeakPtr_Tests {
	void SimpleTest();
	void Expired();
	void Expired2();
	void Lock();

	void TEST_ALL();
};

#endif /* WEAK_PTR_TESTS__INCLUDE_GUARD__H */