//============================================================================
// Name        : Arrays.h
// Created on  : 20.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Arrays tests
//============================================================================

#ifndef ARRAYS_CLASS_TESTS_INCLUDE_GUARD_H_
#define ARRAYS_CLASS_TESTS_INCLUDE_GUARD_H_

namespace Arrays {
	void TEST_ALL();
};

#endif /* ARRAYS_CLASS_TESTS_INCLUDE_GUARD_H_ */
