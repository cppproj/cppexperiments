//============================================================================
// Name        : Exceptions.h
// Created on  : 07.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Exceptions tests
//============================================================================

#include "Exceptions.h"

#include <iostream>
#include <string>
#include <exception>

using String = std::string;
using CString = const String&;

namespace Exceptions::Exceptions_In_Constructors {

	class Base_WithException {
	public :
		Base_WithException() {
			std::cout << "Base_WithException::Base_WithException()" << std::endl;
			throw std::runtime_error("Exception from Base_WithException::Base_WithException()");
		}
		virtual ~Base_WithException() /*noexcept(false)*/ {
			std::cout << "~Base_WithException::Base_WithException()" << std::endl;
		}
	};
	
	class Base {
	public:
		Base() {
			std::cout << "Base::Base()" << std::endl;
		}
		virtual ~Base() /*noexcept(false)*/ {
			std::cout << "Base::~Base()" << std::endl;
		}
	};

	class Base2 {
	public:
		Base2(): Base2("Test") {
			std::cout << "Base2::Base2()" << std::endl;
			throw std::runtime_error("Exception from Base_WithException::Base_WithException()");
		}
		Base2(const std::string& s) {
			std::cout << "Base2::Base2(const std::string& s)" << std::endl;
		}
		virtual ~Base2() noexcept(false) {
			std::cout << "Base2::~Base2()" << std::endl; 
		}
	};

	class Derived: public Base {
	public:
		Derived() {
			std::cout << "Derived::Derived()" << std::endl;
			throw std::runtime_error("Exception from Derived::Derived()");
		}
		virtual ~Derived() /*noexcept(false)*/ {
			std::cout << "Derived::~Derived()" << std::endl;
		}
	};


	class A {
	public:
		A() { std::cout << "A::A()" << std::endl; }
		virtual ~A() { std::cout << "A::~A()" << std::endl; }
	};

	class B {
	public:
		B() { 
			std::cout << "B::B()" << std::endl;
			throw std::runtime_error("Exception from B::B()");
		}
		virtual ~B() { std::cout << "B::~B()" << std::endl; }
	};

	class C{
	public:
		C() { std::cout << "C::C()" << std::endl; }
		virtual ~C() { std::cout << "C::~C()" << std::endl; }
	};


	class TestClass {
	private:
		A a;
		B b;
		C c;

	public:
		TestClass() {
			std::cout << "TestClass::TestClass()" << std::endl;
		}
		virtual ~TestClass() /*noexcept(false)*/ {
			std::cout << "TestClass::~TestClass()" << std::endl;
		}
	};

	class TestClass_Handle_Exceptions {
	private:
		A a;
		B b;
		C c;

	public:
		TestClass_Handle_Exceptions() try {
			std::cout << "TestClass_Handle_Exceptions::TestClass_Handle_Exceptions()" << std::endl;
		} catch (const std::exception& exc) {
			std::cout << "EXCEPTION BEFORE CTOR:  " << exc.what() << std::endl;
		}
	
		virtual ~TestClass_Handle_Exceptions() /*noexcept(false)*/ {
			std::cout << "TestClass_Handle_Exceptions::~TestClass_Handle_Exceptions()" << std::endl;
		}
	};

	///////////////////////////////////////////////////////////////////////////////

	class BadObject {
	public:
		BadObject(int v) {
			std::cout << "BadObject::BadObject()" << std::endl;
			if (v < 0)
				throw std::runtime_error("Exception from BadObject::BadObject()");
		}
		virtual ~BadObject() { 
			std::cout << "BadObject::~BadObject()" << std::endl; 
		}
	};

	class ExcHandlerClass : public BadObject {
	public:
		ExcHandlerClass(int v) try: BadObject(v) {
			std::cout << "ExcHandlerClass::ExcHandlerClass() constructor" << std::endl;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION BEFORE CTOR:  " << exc.what() << std::endl;
		}


		virtual ~ExcHandlerClass() {
			std::cout << "ExcHandlerClass::~ExcHandlerClass()" << std::endl;
		}
	};


	///////////////////////////////////////////////////////////////////////////////

	void Test_Base() {
		try {
			Base_WithException b;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}

	void Test_Base_TwoConstructors() {
		try {
			Base2 b;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}
	

	void Test_DerivedClass() {
		try {
			Derived b;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}

	void Test_Exception_Composition() {
		try {
			TestClass obj;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}

	void Test_Exception_Composition_Handling() {
		try {
			TestClass_Handle_Exceptions obj;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}

	void Test_Exception_InitList() {
		try {
			ExcHandlerClass obj(1);
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}

		try {
			ExcHandlerClass obj(-1);
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}
};


namespace Exceptions::Exceptions_In_Destructor {

	class Base_WithDtorException {
	public:
		Base_WithDtorException() {
			std::cout << "Base_WithException::Base_WithException()" << std::endl;
			
		}
		virtual ~Base_WithDtorException() /*noexcept(false)*/ {
			std::cout << "~Base_WithException::Base_WithException()" << std::endl;
			throw std::runtime_error("Exception from Base_WithException::Base_WithException()");
		}
	};

	///////////////////////////////////////////////////////////////////////////////

	void Test1() {
		try {
			Base_WithDtorException b;
		}
		catch (const std::exception& exc) {
			std::cout << "EXCEPTION:  " << exc.what() << std::endl;
		}
	}
}

namespace Exceptions::ExceptionsTypes {

	void throw_timed_out_exception() {
		throw std::system_error(std::make_error_code(std::errc::timed_out));
	}

	void TimeoutException() {
		try {
			throw_timed_out_exception();
		} catch (const std::exception& e) {
			std::cout << e.what() << std::endl;
		}
	}
}

void Exceptions::TEST_ALL() {
	 // Exceptions_In_Constructors::Test_Base();
	 // Exceptions_In_Constructors::Test_Base_TwoConstructors();

	// Exceptions_In_Constructors::Test_DerivedClass();
	// Exceptions_In_Constructors::Test_Exception_Composition();
	// Exceptions_In_Constructors::Test_Exception_Composition_Handling();
	// Exceptions_In_Constructors::Test_Exception_InitList();

	// Exceptions_In_Destructor::Test1();

	ExceptionsTypes::TimeoutException();
};