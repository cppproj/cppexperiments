//============================================================================
// Name        : Exceptions.h
// Created on  : 07.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Exceptions tests
//============================================================================

#ifndef EXEPTION_HANDLING_TESTS__H_
#define EXEPTION_HANDLING_TESTS__H_

namespace Exceptions {
	void TEST_ALL();
};

#endif /* EXEPTION_HANDLING_TESTS__H_ */
