//============================================================================
// Name        : MoveSemantics_RuleOfFive.h
// Created on  : 1.0
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Move semantics tests (Rule of five)
//============================================================================

#ifndef MOVE_SEMANTICS__INCLUDE_GUARD__H
#define MOVE_SEMANTICS__INCLUDE_GUARD__H

#include <iostream>
#include <string>

using String = std::string;
using CString = const String&;

namespace MoveSemantics_RuleOfFive
{
	void TEST_ALL();
};

#endif /* MOVE_SEMANTICS__INCLUDE_GUARD__H */
