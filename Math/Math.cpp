//============================================================================
// Name        : Math.cpp
// Created on  : 20.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Math tests
//============================================================================

#include "Math.h"
#include <math.h>

#include <iostream>
#include <string>
#include <string_view>
#include <array>

namespace Math {

	void Min() {
		std::cout << "min(1, 3) = " << std::min(1, 3) << std::endl;
		std::cout << "min(1, -3) = " << std::min(1, -3) << std::endl;
	}

	void Max() {
		std::cout << "max(1, 3) = " << std::max(1, 3) << std::endl;
		std::cout << "max(1, -3) = " << std::max(1, -3) << std::endl;
	}

	void Pow() {
		std::cout << "pow(2, 1) = " << std::pow(2, 1) << std::endl;
		std::cout << "pow(2, 2) = " << std::pow(2, 2) << std::endl;
		std::cout << "pow(2, 3) = " << std::pow(2, 3) << std::endl;
		std::cout << "pow(3, 2) = " << std::pow(3, 2) << std::endl;
		std::cout << "pow(3, 3) = " << std::pow(3, 3) << std::endl;
		std::cout << "pow(1.5, 52) = " << std::pow(1.5, 52) << std::endl;
		std::cout << "pow(256, 2) = " << std::pow(256, 2) << std::endl;
	}

	void Sqrt() {
		std::cout << "sqrt(16) = " << std::sqrt(16) << std::endl;
		std::cout << "sqrt(27) = " << std::sqrt(27) << std::endl;
		std::cout << "sqrt(25) = " << std::sqrt(25) << std::endl;
	}

	void Cbrt() {
		std::cout << "cbrt(27) = " << std::cbrt(27) << std::endl;
		std::cout << "cbrt(8) = " << std::cbrt(8) << std::endl;
		std::cout << "cbrt(125) = " << std::cbrt(125) << std::endl;
	}

	void Hypot() {
		std::cout << "hypot(3,4) = " << std::hypot(3, 4) << std::endl;
	}
}

namespace Math::Exponential {

	//  It will calculate the exponential e raised to power p.
	void Exp() {
		std::cout << "exp(5) = " << std::exp(5) << std::endl;
	}

	//  It will calculate the base 2 exponential of p.
	void Exp2()
	{
		std::cout << "exp2(8) = " << exp2(8) << std::endl;
	}

	//  log(p): It will calculate the logarithm of p.
	void Log()
	{
		std::cout << "log(8)= " << log(8) << std::endl;
	}

	//  log2(p): It will calculate the base 2 logarithm of p.
	void Log2()
	{
		std::cout << "log2(8)= " << log2(8) << std::endl;
		std::cout << "log2(32)= " << log2(32) << std::endl;
		std::cout << "log2(128)= " << log2(128) << std::endl;
	}	

	//   It will calculate the common logarithm of p.
	void Log10()
	{
		std::cout << "log10(8) = " << log10(8) << std::endl;
		std::cout << "log10(100) = " << log10(100) << std::endl;
		std::cout << "log10(1000) = " << log10(1000) << std::endl;
	}
}
namespace Math::IntegerFunctions {

	//  it rounds up the value of z.
	void Ceil() {
		std::cout << "ceil(2.3) = " << ceil(2.3) << std::endl;
		std::cout << "ceil(3.8) = " << ceil(3.8) << std::endl;
		std::cout << "ceil(-2.3)= " << ceil(-2.3) << std::endl;
		std::cout << "ceil(-3.8) = " << ceil(-3.8) << std::endl;
	}

	// floor(z): it rounds down the value of z.
	void Floor() {
		std::cout << "floor(2.3) = " << floor(2.3) << std::endl;
		std::cout << "floor(3.8) = " << floor(3.8) << std::endl;
		std::cout << "floor(-2.3)= " << floor(-2.3) << std::endl;
		std::cout << "floor(-3.8) = " << floor(-3.8) << std::endl;
	}

	// fmod(z,y): It calculates the remainder of division z/y.
	void Fmod()
	{
		std::cout << "fmod(5.3, 2) = " << fmod(5.3, 2) << std::endl;
		std::cout << "fmod(18.5, 4.2) = " << fmod(18.5, 4.2) << std::endl;
		std::cout << "fmod(9, 4) = " << fmod(9, 4) << std::endl;
		std::cout << "fmod(9.0, 4.4) = " << fmod(9.0, 4.4) << std::endl;
	}

	// trunc(z): It will round off the z value towards zero.
	void Trunk() {
		std::cout << "trunc(5.4) = " << trunc(5.3) << std::endl;
		std::cout << "trunc(-1.3) = " << trunc(-1.3) << std::endl;
	}

	// Rounds x to an integral value, using the rounding direction specified by fegetround.
	void Nearbyint() {
		std::cout << "nearbyint(2.3) = " << nearbyint(2.3) << std::endl;
		std::cout << "nearbyint(2.6) = " << nearbyint(2.6) << std::endl;
		std::cout << "nearbyint(-2.3) = " << nearbyint(-2.3) << std::endl;
		std::cout << "nearbyint(-3.8) = " << nearbyint(-3.8) << std::endl;
	}

	// Returns the floating-point remainder of numer/denom (rounded to nearest):
	void Remainder() {
		std::cout << "remainder(5.3, 2) = " << remainder(5.3, 2) << std::endl;
		std::cout << "remainder(18.5, 4.2) = " << remainder(18.5, 4.2) << std::endl;
	}
}

void Math::TEST_ALL()
{
	// Min();
	// Max();

	 Pow();
	
	// Sqrt();
	// Cbrt();

	// Hypot(); // hypotenuse of triangle

	// Exponential::Exp();
	// Exponential::Exp2();
	// Exponential::Log();
	// Exponential::Log2();
	// Exponential::Log10();


	// IntegerFunctions::Ceil();
	// IntegerFunctions::Floor();
	// IntegerFunctions::Fmod();
	// IntegerFunctions::Trunk();
	// IntegerFunctions::Nearbyint();
	// IntegerFunctions::Remainder();
}
