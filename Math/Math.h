//============================================================================
// Name        : Math.h
// Created on  : 20.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Math tests
//============================================================================

#ifndef MATH_TESTS_INCLUDE_GUARD_H_
#define MATH_TESTS_INCLUDE_GUARD_H_

namespace Math {
	void TEST_ALL();
};

#endif /* MATH_TESTS_INCLUDE_GUARD_H_ */
