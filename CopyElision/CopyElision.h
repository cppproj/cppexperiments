//============================================================================
// Name        : CopyElision.h
// Created on  : 20.09.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : CopyElision tests
//============================================================================

#ifndef COPY_ELISION_TESTS_INCLUDE_GUARD__H
#define COPY_ELISION_TESTS_INCLUDE_GUARD__H

namespace CopyElision {
	void TEST_ALL();
};

#endif // (!COPY_ELISION_TESTS_INCLUDE_GUARD__H)