//============================================================================
// Name        : InlineVariables.h
// Created on  : 11.09.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : BitSet tests
//============================================================================

#ifndef INLINE_VARIABLES_TESTS__H_
#define INLINE_VARIABLES_TESTS__H_

namespace InlineVariables {
	void TEST_ALL();
};

#endif /* INLINE_VARIABLES_TESTS__H_ */
