//============================================================================
// Name        : FunctionObjects.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Function bbjects tests
//============================================================================

#ifndef FUNCTION_OBJECTS_TESTS__H_
#define FUNCTION_OBJECTS_TESTS__H_

namespace FunctionObjects
{
	void FuncPtr_Tests();
	void Bind_Test_1();
	void Bind_Test_2();
	void Bind_Test_3();

	void Vector_Of_Functions();

	void TEST_ALL();
};

#endif /* FUNCTION_OBJECTS_TESTS__H_ */