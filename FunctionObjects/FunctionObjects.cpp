//============================================================================
// Name        : FunctionObjects.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Function objects tests
//============================================================================

#include <iostream>
#include <string>
#include <functional>
#include <string>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <functional>   // std::modulus, std::bind2nd
#include <algorithm> 
#include <string_view> 
#include <type_traits>
#include "../Integer/Integer.h"
#include "FunctionObjects.h"

using String = std::string;
using CString = const String&;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                       Lambda_Tests                                                                             //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Lambda_Tests {

	class SomeClass {
	private:
		int m_x = 0;

	public:
		void info() const { 
			std::cout << "Info. X = " << m_x << std::endl; 
		}
		void increment() { 
			std::cout << "increment = " << m_x << std::endl;
			this->m_x++; 
			std::cout << "increment = " << m_x << std::endl;
		}

		// C++14
		void Func_C14_Style() {
			// const copy of *this
			auto lambda1 = [self = *this]() mutable {
				self.info();
			};
			// non-copy of copy of *this
			auto lambda2 = [self = *this]() mutable { 
				self.increment(); 
			};

			lambda1();
			lambda2();
			lambda1();
		}

		// C++17
		void Func_C17_Style() {
			// const copy of *this
			auto lambda1 = [*this](){ info(); };
			// non-const copy of *this
			auto lambda2 = [*this]() mutable { increment(); };
			lambda1();
			lambda2();
			lambda1();
		}


		void Func_Tests() {
			info();
			increment();
			info();
		}
	};

	void Pass_THIS_to_Lambda() {
		SomeClass T;

		 T.Func_C14_Style();
		// T.Func_C17_Style();
		// T.Func_Tests();
	}

	//-------------------------------------------------------------------------------//

	void FindIF_Lambda_Test()
	{
		std::vector<int> v = { 1, 2 ,3, 4 ,5 };

		std::cout << "list items:" << std::endl;
		auto print = [](int val) { std::cout << val << std::endl; };
		std::for_each(std::begin(v), std::end(v), print);

		auto is_odd = [](int n) {return n % 2 == 1; };
		std::cout << "\nOdd items:" << std::endl;

		auto oddIter = v.begin();
		while (v.end() != (oddIter = std::find_if(oddIter, std::end(v), is_odd))) {
			std::cout << *oddIter << std::endl;
			oddIter++;
		}
	}

	void Test1()
	{
		std::vector<int> vect = { 1, 2, 3, 4, 5 };
		auto print = [](int val) { std::cout << val << std::endl; };
		std::for_each(vect.begin(), vect.end(), print);
	}

	void Capture_Modes()
	{
		std::cout << "\n------------------------------------------ Test1: ------------------------\n" << std::endl;
		{
			std::string value{ "Some_Text_value" };
			auto function = [&value]() { 
				value = "Some_Text_value_NEW";
				std::cout << value << std::endl; 
			};
			function();
			std::cout << value << std::endl;
		}
		std::cout << "\n------------------------------------------ Test2: ------------------------\n" << std::endl;
		{
			std::string value{ "Some_Text_value" };
			auto function = [=]() {
				// value = "Some_Text_value_NEW";
				std::cout << value << std::endl;
			};
			function();
			std::cout << value << std::endl;
		}
		std::cout << "\n------------------------------------------ Test3: ------------------------\n" << std::endl;
		{
			std::string value{ "Some_Text_value" };
			auto function = [value]() {
				// value = "Some_Text_value_NEW";
				std::cout << value << std::endl;
			};
			function();
			std::cout << value << std::endl;
		}
		std::cout << "\n------------------------------------------ Test3: ------------------------\n" << std::endl;
		{
			std::unique_ptr<Integer> integer = std::make_unique<Integer>(22);
			auto function = [integer = std::move(integer)]() {
				std::cout << integer->getValue() << std::endl;
			};
			function();
		}
	}


	//////////////////////

	struct {
		template<typename T, typename U>
		auto operator()(T x, U y) const { return x + y; }
	} sum{};

	void Lambda_Struct() {
		auto result = sum(1, 2);
		std::cout << result << std::endl;
	}

	/////////////////////////////////////////////////

	// cant use 'const std::string& text' herer
	const auto getFunction(const std::string text) {
		return [text](const std::string& value) { return text + ": " + value;  };
	}

	void Function_ReturnLambda() {
		auto handler = getFunction("Some text");
		std::cout << handler("Value") << std::endl;
	}

	//----------------------------------------------------------------------------------------//

	template<typename ...Args>
	void print(Args&&... args) {
		(std::cout << ... << std::forward<Args>(args)) << std::endl;
	}

	template<typename ...Args>
	auto Sum(Args ...args) {
		return (args + ...);
	}

	template<typename ...Args>
	auto Sum_RLal(Args&&... args) {
		return (std::forward<Args>(args) + ...);
	}

	void Variadic_Lambdas() {
		
		auto foo = [](auto... param) {
			return Sum(param...);
		};

		auto foo_foo = [](auto&&... param) {
			return Sum_RLal(std::forward<decltype(param)>(param)...);
		};

		auto result1 = foo(1, 2, 3 ,4, 5);
		auto result2 = foo_foo(1, 2, 3, 4, 5);

		std::cout << result1 << std::endl;
		std::cout << result2 << std::endl;
	}

	//---------------------------------------------------------------------------------------//

	void Pass_UniquePtr_2Lambda() {
		std::cout << " ------------------Pass by REF test: ------------------" << std::endl;
		{
			auto integer = std::make_unique<Integer>(12);
			auto printer = [&integer] ()-> void { integer->printInfo(); };

			printer();
			integer->printInfo();

			std::cout << "Done" << std::endl;
		}
		std::cout << "\n------------------ Pass by VALUE test: ------------------" << std::endl;
		{
			auto integer = std::make_unique<Integer>(12);
			auto printer = [var = std::move(integer) ]{ var->printInfo(); };

			printer();
			// integer->printInfo(); // Crush HERE!

			std::cout << "Done" << std::endl;
		}
	}

	//-------------------------------------------------------------------------------------------//

	void Statics_In_Lambda() {
		auto counter = [] {
			static int count = 0; 
			return ++count;
		};
		auto c1 = counter, c2 = counter;

		std::cout << c1() << "  " << c1() << "  " << c1() << std::endl;
		std::cout << c2() << "  " << c2() << "  " << c2() << std::endl;
	}

	//-------------------------------------------------------------------------------------------//

	void Capture_Variable_Copy() {
		std::vector<std::function<void()>> handlers;

		{
			int value = 123;
			handlers.emplace_back([&value]() { std::cout << "Value: " << value << std::endl; });
		}

		auto func = handlers.front();
		func();
	}

	//----------------------------------------------------------------------------------------//

	void Lambda_With_Params_Initialization() {
		auto func = [ptr = std::make_unique<Integer>(0)]()-> void {
			ptr->increment();
			ptr->printInfo();
		};

		func();
		func();
		func();
	}

	//----------------------------------------------------------------------------------------//

	void Determine_TypeOf_VectorParameter() {
		auto func = [](const auto& vect)-> void {
			std::cout << typeid(vect.back()).name() << std::endl;

			using T = decltype(vect.back());
			T a{ 1 };

			std::cout << typeid(a).name() << std::endl;
		};


		auto func2 = [](const auto& vect)-> void {
			using V = std::decay_t<decltype(vect)>;
			using T = typename V::value_type;
			T a{ 1 };

			std::cout << typeid(a).name() << std::endl;
		};
;
		const std::vector<int> numbers ;
		func(numbers);
		func2(numbers);
	}

	//---------------------------------------------------------------------------------------//

	int x = 123;

	auto func1 = [=]() noexcept -> int { return x + 1; };
	auto func2 = [x = x]() noexcept -> int { return x + 1; };

	void Handle_Global_Varibles() {
		x = 10;
		std::cout << "func1 = " << func1() << "   func2 = " << func2() << std::endl;
	}

	void Handle_Global_Varibles2() {
		int x = 10;
		std::cout << "func1 = " << func1() << "   func2 = " << func2() << std::endl;
	}

	//------------------------------------------------------------------------------------------------------------------------//

	void Lambda_Collection() {
		std::vector<std::function<std::string(const std::string&)>> handlers;

		handlers.emplace_back(getFunction("Handler 1"));
		handlers.emplace_back(getFunction("Handler 2"));
		handlers.emplace_back(getFunction("Handler 3"));
		handlers.emplace_back(getFunction("Handler 4"));
		handlers.emplace_back(getFunction("Handler 5"));

		for (const auto& param : { "Value1","Value2" ,"Value2" }) {
			std::for_each(handlers.begin(), handlers.end(), [&param](const auto& func) {
				std::cout << func(param) << std::endl;
			});
			std::cout << std::endl;
		}
	}

	//----------------------------------------------------------------------------------------------------------------------------//

	void Lambda_Itialyzed_With_Another_Lambda() {
		auto l = [
			i = [] {
				struct S {
					int value = 5;
					S() { std::cout << "Constucted!!!\n"; }
					S(const S& obj) { std::cout << "Copied!!!\n"; }
					~S() { std::cout << "Destructed!!!\n"; }
				};
				return S{};
			}()
		] {
			return i;
		};

		auto x = l().value;

		std::cout << x << std::endl;
	}

	//----------------------------------------------------------------------------------------------------------------------------//

	void LambdasWithDestructors() {
		auto l = [i = [] {return 5; }]{
			return i;
		};

		auto x = l()();
		std::cout << x << std::endl;
	}

	//----------------------------------------------------------------------------------------------------------------------------//

	template<typename L1, typename L2>
	struct S : L1, L2 {
		S(L1 l1, L2 l2) : L1(std::move(l1)), L2(std::move(l2)) { }
		using L1::operator();
		using L2::operator();
	};


	void Inheriting_From_Lambdas() {
		auto func1 = []()-> std::string {return "Hello world!"; };
		auto func2 = [](const std::string& text)-> void { std::cout << "Input text: " << text << std::endl;  };
		auto compose = S(func1, func2);

		//compose("123");
		compose();
	}

	//----------------------------------------------------------------------------------------------------------------------------//

	template<typename... Args>
	void printer(Args&&... args) {
		std::cout << "Args count = " << sizeof ... (args) << std::endl;
		(std::cout << ... << std::forward<Args>(args)) << std::endl;
	}

	void Template_Lambda() 
	{
		// generic lambda, operator() is a template with two parameters
		auto glambda_auto = [](auto a, auto&& b) { 
			return a < b; 
		};

		// generic lambda, operator() is a template with two parameters
		auto glambda_template = []<class T>(T a, auto && b) { 
			return a < b; 
		};

		auto printer_lambda = []<typename ...Ts>(Ts&& ...ts) {
			return printer(std::forward<Ts>(ts)...);
		};

		//------------------------------------- Tests ----------------------------------------/

		{
			bool b = glambda_auto(3, 3.14); // ok
			std::cout << "3 < 3.14 == " << std::boolalpha << b << std::endl;
		}

		{
			bool b = glambda_template(3, 3.14); // ok
			std::cout << "3 < 3.14 == " << std::boolalpha << b << std::endl;
		}

		{
			printer_lambda(1, "One", 2.44);
		}
	}

	void Template_Lambda_Default()
	{
		auto print = []<typename T> (T a) -> void {
			std::cout << typeid(T).name() << std::endl;
		};

		print(1);
	}

	void _TEST_() {
		[n = 10, text = std::string("Some string")] () {
			std::cout << text << ' ' << n << std::endl;
		} ();

		auto lambdaVectorIntegral = []<std::integral T>(const std::vector<T>&vec) { return vec.size(); };
	}
};

namespace FunctionObjects {

	class SumFunctor {
	public:
		int operator()(int a, int b) {
			return a + b;
		}
	};


	void Vector_Of_Functions()
	{
		const std::vector<int> numbers = { 1, 2, 3, 4, 5, 6, 7 };
		std::vector<std::function<int(int)>> handlers;

		handlers.emplace_back([](int value) { return value * 2; });
		handlers.emplace_back([](int value) { return value * 4; });
		handlers.emplace_back([](int value) { return value * 10; });

		for (const auto& func : handlers) {
			for (int v : numbers) {
				std::cout << func(v) << " ";
			}
			std::cout << std::endl;
		}
	}

	void FunctorTest() {
		SumFunctor sum_func;
		int result = sum_func(1, 3);
		std::cout << result << std::endl;
	}


	bool isvowel(char c) {
		return std::string("aeoiuAEIOU").find(c) != std::string::npos;
	}


	////////////////////////////////////////////////////////////////////////////////

	namespace Less {
		template <typename A, typename B, typename U = std::less<>>
		bool compare(A a, B b, U comparer = U())
		{
			return comparer(a, b);
		}

		void Test() {
			std::cout << std::boolalpha;
			std::cout << compare(5, 20) << std::endl;
			std::cout << compare(100, 10) << std::endl;
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	namespace Greater {
		template <typename A, typename B, typename U = std::greater<>>
		bool compare(A a, B b, U comparer = U())
		{
			return comparer(a, b);
		}

		void Test() {
			std::cout << std::boolalpha;
			std::cout << compare(5, 20) << std::endl;
			std::cout << compare(100, 10) << std::endl;
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	namespace Multiplies {
		template <typename T, typename U = std::multiplies<>>
		T mult(T a, T b, U func = U())
		{
			return func(a, b);
		}

		void Test() {
			std::cout << mult(5, 20) << std::endl;
			std::cout << mult(100, 10) << std::endl;
		}

		void Test2() {
			int factorials[9], numbers[9] = { 1,2,3,4,5,6,7,8,9 };
			std::partial_sum(numbers, numbers + 9, factorials, std::multiplies<int>());
			for (int i = 0; i < 9; i++)
				std::cout << numbers[i] << "! is " << factorials[i] << std::endl;
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	namespace Negate {
		template <typename T, typename U = std::negate<>>
		T __negate(T a, U func = U()) {
			return func(a);
		}

		void Test() {
			std::cout << __negate(-2) << std::endl;
			std::cout << __negate(92) << std::endl;
		}

		void Test2() {
			int numbers[] = { 1, -2, 3 };
			std::transform(numbers, numbers + 3, numbers, std::negate<int>());
			for (int i : numbers)
				std::cout << i << ' ';
			std::cout << std::endl;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	namespace Modulus {

		template <typename T>
		T modulus(T a, T b) {
			return a % b;
		}

		//template <typename T = int>
		int modulus2(int x, int y) {
			return x % y;
		}

		
		void Test() {
			std::cout << Modulus::modulus(5, 2) << std::endl;
		}

		
		void Test2() {
	
			{
				int numbers[] = { 1,2,3,4,5 };
				int remainders[5];
				std::transform(numbers, numbers + 5, remainders, std::bind(modulus2, std::placeholders::_1, 2));
				for (int i = 0; i < 5; i++)
					std::cout << numbers[i] << " is " << (remainders[i] == 0 ? "even" : "odd") << '\n';
			}

			std::cout << "\nTest2:\n" << std::endl;
			{
				int numbers[] = { 1,2,3,4,5 };
				// int remainders[5];
				const auto is_odd = [](int v) { if (0 == modulus(v, 2)) std::cout << "even" << std::endl; else std::cout << "odd" << std::endl; };
				std::for_each(numbers, numbers + 5, is_odd);
			}

		}
	}
}

namespace FunctionObjects::BindTests {

	class Utilities {
	public:

		void Info() {
			std::cout << "Utilities::Info()" << std::endl;
		}

		void printer(const std::string& text) {
			std::cout << __FUNCTION__ << ". Text: " << text << std::endl;
		}
		void printer_two_params(std::string_view prefix, std::string_view text) {
			std::cout << prefix << " : " << text << std::endl;
		}
	};


	double my_divide(double x, double y) {
		return x / y;
	}

	void printer(CString text) {
		std::cout << "Input text 2: " << text << std::endl;
	}

	class Object {
	public:
		void info(CString text) {
			std::cout << "Input text: " << text << std::endl;
		}
	};

	struct Foo {
		Foo(int num) : num_(num) {}
		void print_add(int i) const {
			std::cout << num_ + i << '\n';
		}
		int num_;
	};

	void printe_ext(const std::string& text)
	{
		std::cout << __FUNCTION__ << ". Text: " << text << std::endl;
	}


	//////////////////////////////// TESTS ///////////////////////////////////////////////

	void FuncPtr_Tests() {

		void(*ptrFunc1)(const std::string& str);
		ptrFunc1 = &printe_ext;
		ptrFunc1("Old style test 1");

		auto prtFunc2 = &printe_ext;
		ptrFunc1("Old style test 2");

		using prtFunc3 = void (Utilities::*)(const std::string& str);
		prtFunc3 ptr3 = &Utilities::printer;

		Utilities utils;
		(utils.*ptr3)("Old style test 3");

		const auto auto_printer = [](const std::string& text1, const std::string& text2)-> void {
			std::cout << text1 << " . " << text2 << std::endl;
		};

		std::function<void()> func1 = std::bind(auto_printer, "std::function", "Test1");
		func1();

		std::function<void(const std::string&)> func2 = std::bind(auto_printer, "std::function", std::placeholders::_1);
		func2("Test2");


		std::function<void(const std::string&)> func3 = std::bind(&Utilities::printer, new Utilities(), std::placeholders::_1);
		func3("new Utilities()->printer() called.");


		auto print = std::mem_fn(&Utilities::printer);
		print(utils, "The TEST 123");
	}

	void Bind_Test_1() {

		auto fn_five = std::bind(my_divide, 10, 2);
		std::cout << fn_five() << std::endl;

		auto fn_half = std::bind(my_divide, std::placeholders::_1, 5);
		std::cout << fn_half(10) << std::endl;
	}

	void Bind_Test_2() {
		std::unique_ptr<Object> object = std::unique_ptr<Object>(new Object());

		auto func = std::bind(&Object::info, object.get(), std::placeholders::_1);
		func("World");

		auto func1 = std::bind(&Object::info, object.get(), "Predefined Text");
		func1();

		auto func2 = std::bind(&printer, "Text for printer");
		func2();
	}

	void Bind_Test_3() {
		const Foo foo(314159);

		std::function<void(int)> f_add_display2 = std::bind(&Foo::print_add, &foo, std::placeholders::_1);
		f_add_display2(2);
	}

	void Bind_Test_4() {
		Utilities utilities;
		{
			auto printer = std::bind(&Utilities::printer_two_params, &utilities, "Binded_Prefix", std::placeholders::_1);
			printer("Tes3");
		}
		std::cout << "\n-------------------------  Test2: -----------------------------\n" << std::endl;
		{
			std::vector<std::string> strings = { "Value1", "Value2", "Value3", "Value4", "Value5" };
			auto printer = std::bind(&Utilities::printer_two_params, &utilities, "Binded_Prefix", std::placeholders::_1);
			std::for_each(strings.begin(), strings.end(), printer);
		}
	}

	void Bind_Class_Method() {
		Utilities utils;

		auto info_func = std::bind(&Utilities::Info, &utils);
		info_func();

		auto func1 = std::bind(&Utilities::printer_two_params, &utils, std::placeholders::_1, std::placeholders::_2);
		func1("PARAM_1", "PARAM_2");

		auto func2 = std::bind(&Utilities::printer_two_params, &utils, std::placeholders::_2, std::placeholders::_1);
		func2("PARAM_1", "PARAM_2");

	}

	void Bind_In_Bind() {
		constexpr auto printer = [](const std::string& str1, const std::string& str2)-> void {
			std::cout << "printer() called with params: {" << str1 << ", " << str2 << "}" << std::endl;
			std::cout << str1 << " " << str2 << std::endl;
		};
		constexpr auto getter = [](const std::string& str)-> std::string {
			std::cout << "getter() called with params: {" << str << "}" << std::endl;
			return "[" + str + "]";
		};

		auto callable = std::bind(printer, std::bind(getter, "Test_Prefix"), std::placeholders::_1);
		callable("Input value");
	}

	/***********************************************************************************************************/

	class Base {
	public:
		virtual void print(std::string_view text) const noexcept = 0;
	};

	class Worker1 : public Base {
		virtual void print(std::string_view text) const noexcept override {
			std::cout << "Worker1::print(" << text << ") called." << std::endl;
		}
	};

	class Worker2 : public Base {
		virtual void print(std::string_view text) const noexcept override {
			std::cout << "Worker2::print(" << text << ") called." << std::endl;
		}
	};

	void Bind_CallDifferentClassMethods() {
		std::cout << " ------------------------------------------ Using Bind ----------------------------------------" << std::endl;
		{
			auto callable = std::bind(&Base::print, std::placeholders::_1, "Some_Test_Value");
			std::vector<std::shared_ptr<Base>> objects = { std::make_shared<Worker1>(), std::make_shared<Worker2>() };
			std::for_each(objects.begin(), objects.end(), callable);
		}
		std::cout << " ------------------------------------------ Using Lambda ----------------------------------------" << std::endl;
		{
			auto callable = [](auto obj) { obj->print("Some_Test_Value"); };
			std::vector<std::shared_ptr<Base>> objects = { std::make_shared<Worker1>(), std::make_shared<Worker2>() };
			std::for_each(objects.begin(), objects.end(), callable);
		}
	}
}

namespace FunctionObjects::ClassMethodPointers {

	class Base {
	private:
		std::string value;

	public:
		Base(const std::string& str) : value(str) {
		}
		Base(const Base& obj) : value(obj.value) {
			std::cout << "Base::Base(" << value << ") copy contructor." << std::endl;
		}
		Base(Base&& obj) noexcept : value(std::move(obj.value)) {
			std::cout << "Base::Base(" << value << ") move contructor." << std::endl;
		}
		Base& operator=(const Base& obj)  {
			std::cout << "Base::Base(" << value << ")." << std::endl;
			if (this != &obj) {
				value = obj.value;
			}
			return *this;
		}
		Base& operator=(Base&& obj) noexcept {
			std::cout << "Move assignment oprator Base::Base(" << value << ")." << std::endl;
			if (this != &obj) {
				value = std::move(obj.value);
			}
			return *this;
		}

	public:
		virtual void doSomething(const std::string& str) {
			std::cout << __FUNCTION__ << std::endl;
			std::cout << str << std::endl;
		}
	};

	class Derived : public Base {
	public:
		Derived(const std::string& str) : Base(str) {
		}

	public:
		virtual void doSomething(const std::string& str) override {
			std::cout << __FUNCTION__ << std::endl;
			std::cout << str << std::endl;
		}
	};

	/***********************************************************************************************************************************/

	void CallMethod_ByPointer() {
		using methodPtr = void (Base::*)(const std::string& str);
		methodPtr func = &Base::doSomething;

		{
			std::cout << "\n----------------------------- Calling Base class method by pointer: ---------------------\n" << std::endl;
			Base obj("Test");
			(obj.*func)("Some input parameter");
		}
		{
			std::cout << "\n----------------------------- Calling Derived class method by pointer: ---------------------\n" << std::endl;
			Derived obj("Test");
			(obj.*func)("Some input parameter");
		}
	}

	void CallMethod_BIND() {
		{
			std::cout << "\n----------------------------- Calling Base class method using BIND: ---------------------\n" << std::endl;
			Base obj("Test");
			auto func = std::bind(&Base::doSomething, &obj, std::placeholders::_1);
			func("Some input parameter");
		}
		{
			std::cout << "\n----------------------------- Calling Derived class method using BIND: ---------------------\n" << std::endl;
			Derived obj("Test");
			auto func = std::bind(&Base::doSomething, &obj, std::placeholders::_1);
			func("Some input parameter");
		}
	}

	void CallMethod_Mem_Fn() {
		auto func = std::mem_fn(&Base::doSomething);
		{
			std::cout << "----------------------------- Calling Base class method using std::mem_fn: ---------------------\n" << std::endl;
			Base obj("Test");
			func(obj, "Some input parameter");
		}
		{
			std::cout << "\n----------------------------- Calling Derived class method using std::mem_fn: ---------------------\n" << std::endl;
			Derived obj("Test");
			func(obj, "Some input parameter");
		}
	}
}

namespace FunctionObjects::Auto {

	template<typename T>
	auto get_as_int(T val)-> int {
		return val;
	}

	template<typename T>
	auto get(T val) // -> decltype(typeid(T)) {
	{
		std::cout << typeid(T).name() << std::endl;
		return val;
	}

	void Return_Type_Hint() {

		std::cout << get_as_int(12) << std::endl;
		std::cout << get_as_int(1.232) << std::endl;

		std::cout << std::endl;

		std::cout << get(12) << std::endl;
		std::cout << get(1.232) << std::endl;
	}

}

namespace FunctionObjects {
	void TEST_ALL()
	{

		// Auto::Return_Type_Hint();

		// BindTests::FuncPtr_Tests();
		// BindTests::Bind_Test_1();
		// BindTests::Bind_Test_2();
		// BindTests::Bind_Test_3();
		// BindTests::Bind_Test_4();
		// BindTests::Bind_In_Bind();
		// BindTests::Bind_CallDifferentClassMethods();
		// BindTests::Bind_Class_Method();

		// ClassMethodPointers::CallMethod_ByPointer();
		// ClassMethodPointers::CallMethod_BIND();
		// ClassMethodPointers::CallMethod_Mem_Fn();

		// Vector_Of_Functions();
		// FunctorTest();

		// Less::Test();
		// Greater::Test();

		// Multiplies::Test();
		// Multiplies::Test2();

		// Negate::Test();
		// Negate::Test2();

		// Modulus::Test();
		// Modulus::Test2();

		// Lambda_Tests::FindIF_Lambda_Test();
		// Lambda_Tests::Test1();
		// Lambda_Tests::Pass_THIS_to_Lambda();
		// Lambda_Tests::Capture_Modes();
		// Lambda_Tests::Capture_Variable_Copy();
		// Lambda_Tests::Lambda_With_Params_Initialization();
		// Lambda_Tests::Lambda_Struct();
		// Lambda_Tests::Lambda_Collection();
		// Lambda_Tests::Function_ReturnLambda();
		// Lambda_Tests::Variadic_Lambdas();
		// Lambda_Tests::Pass_UniquePtr_2Lambda();
		// Lambda_Tests::Statics_In_Lambda();
		// Lambda_Tests::Handle_Global_Varibles();
		// Lambda_Tests::Handle_Global_Varibles2();

		Lambda_Tests::Determine_TypeOf_VectorParameter();

		// Lambda_Tests::Lambda_Itialyzed_With_Another_Lambda();
		// Lambda_Tests::LambdasWithDestructors();
		// Lambda_Tests::Inheriting_From_Lambdas();

		// Lambda_Tests::Template_Lambda();
		// Lambda_Tests::Template_Lambda_Default();

		// Lambda_Tests::_TEST_();
	}
}
