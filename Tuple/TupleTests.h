//============================================================================
// Name        : TupleTests.h
// Created on  : 17.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TupleTests testing 
//============================================================================

#ifndef TUPLE_TESTS_INCLUDE_GUARD__H
#define TUPLE_TESTS_INCLUDE_GUARD__H

namespace Tuple_Tests {
	void TEST_ALL();
}

#endif // !TUPLE_TESTS_INCLUDE_GUARD__H