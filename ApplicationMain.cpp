//============================================================================
// Name        : StructuralBindings.cpp
// Created on  : April 22, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : StructuralBindings
//============================================================================

#include <iostream>
#include <string>

#include <array>
#include <cassert>
#include <thread>
#include <execution>

#include <map>
#include <vector>

#include "Initialization/Initialization.h"
#include "FilesStreams/FilesStreams.h"
#include "Literals/Literals.h"
#include "CopyElision/CopyElision.h"
#include "Math/Math.h"
#include "Concepts/Concepts.h"
#include "Arrays/Arrays.h"
#include "Asserts/Asserts.h"
#include "Streams/Streams.h"
#include "Utilities/Utilities.h"
#include "Exceptions/Exceptions.h"
#include "NumericLimits/NumericLimits.h"
#include "EnumTests/EnumTests.h"
#include "Byte/Byte.h"
#include "TypeTraits/TypeTraits.h"
#include "ConstConstexprMutable/ConstConstexprMutable.h"
#include "Chrono/Chrono.h"
#include "Memory/Memory.h"
#include "BitSet/BitSet.h"
#include "Atomic/Atomic.h"
#include "InitializerList/InitializerList.h"
#include "Locale/Locale.h"
#include "ConsoleInOut/ConsoleInOut.h"
#include "StringStream/StringStreamTests.h"
#include "TestSupport/TestSupport.h"
#include "Integer/Integer.h"
#include "InlineVariables/InlineVariables.h"
#include "Any/Any.h"
#include "OptionalTests/OptionalTests.h"
#include "Variant/VariantTests.h"
#include "Algorithms/Algorithms.h"
#include "Templates/Templates.h"
#include "PolymorphicMemoryResources/PolymorphicMemoryResources.h"
#include "MoveSemantics_RuleOfFive/MoveSemantics_RuleOfFive.h"
#include "FileSystemTests/FileSystemTests.h"
#include "Tuple/TupleTests.h"
#include "IteratorTests/IteratorTests.h"
#include "SharedPtr/SharedPtrTests.h"
#include "UniquePtr/UniquePtrTests.h"
#include "WeakPtr/WeakPtrTests.h"
#include "StringView/StringViewTests.h"
#include "FunctionObjects/FunctionObjects.h"
#include "ReferenceWrapper/ReferenceWrapperTests.h"
#include "TypeCast/TypeCastTests.h"
#include "StructuredBinding/StructuredBinding.h"
#include "ObjectOrientedProgramming/ObjectOrientedProgramming.h"
#include "String/String.h"
#include "BitwiseOperation/BitwiseOperation.h"
#include "Span/Span.h"
#include "Ranges/Ranges.h"

#define TEST(nspace) nspace::TEST_ALL();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												    Nodiscard_Fallthrough_Maybe_Unused :										     	    	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Nodiscard_Fallthrough_Maybe_Unused {

	enum class Color {
		black = 0,
		white = 1,
		white1 = 1,
		gray = 2,
	};

	void PrintColor(Color color) {
		switch (color) {
		case Color::gray:
			std::cout << "white + " << std::endl;
			[[fallthrough]];
		case Color::black:
			std::cout << "black" << std::endl;
			break;
		case Color::white:
			std::cout << "white" << std::endl;
			break;
		}
	}

	void Fallthrough_Test() {
		std::cout << "Color::black" << std::endl;
		const Color color1 = Color::black;
		PrintColor(color1);

		std::cout << "\nColor::black" << std::endl;
		const Color color2 = Color::white;
		PrintColor(color2);

		std::cout << "\nColor::gray" << std::endl;
		const Color color3 = Color::gray;
		PrintColor(color3);
	}

	[[nodiscard]]
	unsigned short GetResultCode() {
		return 101;
	}

	unsigned short DoSystemCall() {
		return 1;
	}

	void NoDiscardTest(void) {
		// We should get the WARNING here
		GetResultCode();
		// No WARNING here
		auto resultCode = GetResultCode();
		(void)resultCode;
	}

	void MyBeUnused() {
		// We should get the 'Unused variable' WARNING here
		auto resultCode1 = DoSystemCall();

		// No WARNING here
		[[maybe_unused]]
		auto resultCode2 = DoSystemCall();
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                      Multithreading_Tests                                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Multithreading_Tests {

	void Parallel_Execution_Test() {
		std::vector<int> vec = { 3, 2, 1, 4, 5, 6, 10, 8, 9, 4 };

		//std::sort(vec.begin(), vec.end());                            // sequential as ever
		//std::sort(std::execution::seq, vec.begin(), vec.end());       // sequential
		//std::sort(std::execution::par, vec.begin(), vec.end());       // parallel
		std::sort(std::execution::par_unseq, vec.begin(), vec.end());   // parallel and vectorized

		std::for_each(vec.begin(), vec.end(), [](int v) {std::cout << v << std::endl; });
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                       IntegerSequence_Tests                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace IntegerSequence_Tests {

	// Convert array into a tuple
	template<typename Array, std::size_t... Index>
	auto a2t_impl(const Array& a, std::index_sequence<Index...>) {
		return std::make_tuple(a[Index]...);
	}

	template<typename T, std::size_t N, typename Indices = std::make_index_sequence<N>>
	auto a2t(const std::array<T, N>& a) {
		std::cout << "N = " << N << std::endl;
		return a2t_impl(a, Indices{});
	}

	template<class Ch, class Tr, class Tuple, std::size_t... Is>
	void print_tuple_impl(std::basic_ostream<Ch, Tr>& os, const Tuple& t, std::index_sequence<Is...>) {
		((os << (Is == 0 ? "" : ", ") << std::get<Is>(t)), ...);
	}

	template<class Ch, class Tr, class... Args>
	auto& operator<<(std::basic_ostream<Ch, Tr>& stream, const std::tuple<Args...>& tuple) {
		stream << "(";
		print_tuple_impl(stream, tuple, std::index_sequence_for<Args...>{});
		return stream << ")";
	}


	template<typename Func, typename Tup, std::size_t... index>
	decltype(auto) invoke_helper(Func&& func, Tup&& tup, std::index_sequence<index...>) {
		return func(std::get<index>(std::forward<Tup>(tup))...);
	}

	template<typename Func, typename Tup>
	decltype(auto) invoke(Func&& func, Tup&& tup) {
		constexpr auto Size = std::tuple_size<typename std::decay<Tup>::type>::value;
		return invoke_helper(std::forward<Func>(func),
			std::forward<Tup>(tup),
			std::make_index_sequence<Size>{});
	}

	template <class Tuple, size_t... Is>
	constexpr auto take_front_impl(Tuple tuple, std::index_sequence<Is...>) {
		return std::make_tuple(std::get<Is>(tuple)...);
	}

	template <size_t N, class Tuple>
	constexpr auto take_front(Tuple tuple) {
		return take_front_impl(tuple, std::make_index_sequence<N>{});
	}

	template<class... Args>
	constexpr auto take_all(const std::tuple<Args...>& tuple) {
		return take_front_impl(tuple, std::index_sequence_for<Args...>{});
	}

	template<class Tuple, std::size_t... Is>
	void printer(const Tuple& t, std::index_sequence<Is...>) {
		((std::cout << std::get<Is>(t) << std::endl), ...);
	}

	template<class... Args>
	constexpr auto print_tuple(const std::tuple<Args...>& tuple) {
		printer(tuple, std::index_sequence_for<Args...>{});
	}

	////////////////////////////

	void Print_Tupple_Test() {
		print_tuple(std::make_tuple(1, 2, 3, 5));
	}

	void Tuple_TakeFront() {
		auto t = take_front<2>(std::make_tuple(1, 2, 3, 4));
		assert(t == std::make_tuple(1, 2));

		auto t1 = take_all(std::make_tuple(1, 2, 3, 5));
		assert(t1 == std::make_tuple(1, 2, 3, 4));
	}

	void Array_To_Tupple() {
		std::array<int, 4> array = { 1,2,3,4 };

		// convert an array into a tuple
		auto tuple = a2t(array);
		static_assert(std::is_same<decltype(tuple), std::tuple<int, int, int, int>>::value, "");

		// print it to cout
		std::cout << tuple << std::endl;
	}
}

int main(int argc, char** argv) {

	// TEST(Algorithms);
	// TEST(Any_Tests);
	// TEST(Arrays);
	// TEST(Asserts);
	// TEST(Atomic);
	// TEST(Locale);
	// TEST(Byte);
	// TEST(BitwiseOperation);
	// TEST(BitSet);
	// TEST(Chrono);
	// TEST(Concepts);
	// TEST(EnumTests);
	// TEST(ConsoleInOut);
	// TEST(ConstConstexprMutable);
	// TEST(ObjectOrientedProgramming);
	// TEST(OptionalParamsTests);
	// TEST(FunctionObjects);
	// TEST(FilesStreams);
	// TEST(FileSystem_Tests);
	// TEST(Memory);
	// TEST(Math);
	// TEST(MoveSemantics_RuleOfFive);
	// TEST(CopyElision);
	// TEST(Initialization);
	// TEST(InitializerList);
	// TEST(InlineVariables);
	// TEST(IteratorTests);
	// TEST(Literals);
	// TEST(NumericLimits);
	// TEST(PolymorphicMemoryResources);
	// TEST(ReferenceWrapperTests);
	// TEST(Ranges);
	// TEST(Span);
	// TEST(Streams);
	// TEST(StringStreamTests);
	 TEST(StringTests);
	// TEST(StringView);
	// TEST(StructuredBinding);
	// TEST(TypeCastTests);
	// TEST(Templates);
	// TEST(TypeTraits);
	// TEST(Tuple_Tests);
	// TEST(Variant);
	// TEST(Utilities);
	// TEST(SharedPtr_Tests);
	// TEST(WeakPtr_Tests);
	// TEST(UniquePtr_Tests);

	// If_Else_Cpp17_Tests::Test_If();
	// If_Else_Cpp17_Tests::Test_If_Map();

	// Nodiscard_Fallthrough_Maybe_Unused::Fallthrough_Test();
	// Nodiscard_Fallthrough_Maybe_Unused::NoDiscardTest();
	// Nodiscard_Fallthrough_Maybe_Unused::MyBeUnused();

	// EndAndBeginTests::Test1();
	// EndAndBeginTests::Test2();

	// Multithreading_Tests::Parallel_Execution_Test();

	// IntegerSequence_Tests::Array_To_Tupple();
	// IntegerSequence_Tests::Tuple_TakeFront();
	// IntegerSequence_Tests::Print_Tupple_Test();

	std::this_thread::sleep_for(std::chrono::seconds(100));
}