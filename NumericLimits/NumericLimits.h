//============================================================================
// Name        : NumericLimits.h
// Created on  : 02.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Numeric limits tests
//============================================================================

#ifndef NUMERIC_LIMITS_TESTS_INCLUDE_GUARD__H
#define NUMERIC_LIMITS_TESTS_INCLUDE_GUARD__H

namespace NumericLimits {
	void TEST_ALL();
};

#endif // (!NUMERIC_LIMITS_TESTS_INCLUDE_GUARD__H)