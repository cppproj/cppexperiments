//============================================================================
// Name        : PolymorphicMemoryResources.h
// Created on  : 12.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Polymorphic Memory Resources testsd 
//============================================================================

#ifndef POLYMORPHIC_MEMORY_RESOURCES_INCLUDE_GUARD__H
#define POLYMORPHIC_MEMORY_RESOURCES_INCLUDE_GUARD__H

namespace PolymorphicMemoryResources {
	void TEST_ALL();
};

#endif //!POLYMORPHIC_MEMORY_RESOURCES_INCLUDE_GUARD__H