//============================================================================
// Name        : UniquePtrTests.h
// Created on  : 23.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Unique PTR C++  tests
//============================================================================

#ifndef UNIQUE_PTR_TESTS__INCLUDE_GUARD__H
#define UNIQUE_PTR_TESTS__INCLUDE_GUARD__H

namespace UniquePtr_Tests {
	void UniquePtr_Test1();
	void Swap_Test();
	void Release_Test();
	void Get_Test();
	void Reset_Deleter();
	void Deleter_Test1();
	void Deleter_Test2();
	void Deleter_Test3();
	void Get_Deleter_Test();
	void VariousTests();
	void MakeUnique_ObjectRequirements();

	void TEST_ALL();
};

#endif /* UNIQUE_PTR_TESTS__INCLUDE_GUARD__H */
