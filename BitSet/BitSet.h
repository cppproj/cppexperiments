//============================================================================
// Name        : BitSet.h
// Created on  : 24.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : BitSet tests
//============================================================================

#ifndef BITSET_TESTS__H_
#define BITSET_TESTS__H_

namespace BitSet
{
	void TEST_ALL();
};

#endif /* BITSET_TESTS__H_ */

