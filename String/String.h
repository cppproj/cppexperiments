//============================================================================
// Name        : String.h
// Created on  : 30.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : String libraty tests
//============================================================================

#ifndef STRING_INCLUDE_GUARD__H
#define STRING_INCLUDE_GUARD__H

namespace StringTests {
	void TEST_ALL();
};

#endif /* STRING_INCLUDE_GUARD__H */
