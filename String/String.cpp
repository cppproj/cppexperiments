//============================================================================
// Name        : String.cpp
// Created on  : 30.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : String libraty tests
//============================================================================

#define _CRT_SECURE_NO_WARNINGS


#include <charconv>
#include <iostream>
#include <string>
#include <sstream>
#include <string_view>
#include <cctype>
#include <algorithm>
#include <functional>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>
#include <iostream>
#include <vector>
#include <chrono>
#include <iterator>
#include <regex>
#include <iomanip>

#include "String.h"

namespace StringTests {
	void Create_Test() {

		std::string s0("Initial string");

		// constructors used in the same order as described above:
		std::string s1;
		std::string s2(s0);
		std::string s3(s0, 8, 6);
		std::string s4("A character sequence");
		std::string s5("Another character sequence", 12);
		std::string s7(10, 'x');
		std::string s8(10, 42);      // 42 is the ASCII code for '*'
		std::string s9(s0.begin(), s0.begin() + 7);

		std::cout << "s1: " << s1 << std::endl;
		std::cout << "s2: " << s2 << std::endl;
		std::cout << "s3: " << s3 << std::endl;
		std::cout << "s4: " << s4 << std::endl;
		std::cout << "s5: " << s5 << std::endl;
	    std::cout << "s7: " << s7 << std::endl;
		std::cout << "s8: " << s8 << std::endl;
		std::cout << "s9: " << s9 << std::endl;
	}

	void Itearate()
	{
		std::string str("Test string");
		for (std::string::iterator it = str.begin(); it != str.end(); ++it)
			std::cout << *it;
		std::cout << std::endl;
	}

	void ItearateBackward_RbeginRend()
	{
		std::string str("now step live");
		for (std::string::reverse_iterator rit = str.rbegin(); rit != str.rend(); ++rit)
			std::cout << *rit;
	}

	void Crbegin_Crend_Test()
	{
		std::string str("123 456 789");
		for (auto rit = str.crbegin(); rit != str.crend(); ++rit)
			std::cout << *rit;
		std::cout  << std::endl;
	}

	void Append()
	{
		std::string str;
		std::string str2 = "Writing ";
		std::string str3 = "print 10 and then 5 more";


		str.append(str2);                       // "Writing "
		std::cout << str << std::endl;

		str.append(str3, 6, 3);                   // "10 "
		std::cout << str << std::endl;

		str.append("dots are cool", 5);          // "dots "
		std::cout << str << std::endl;
		
		str.append("here: ");                   // "here: "
		std::cout << str << std::endl;
		
		str.append(10u, '.');                    // ".........."
		std::cout << str << std::endl;
		
		str.append(str3.begin() + 8, str3.end());  // " and then 5 more"
		std::cout << str << std::endl;

		// str.append<int>(5, 0x2E);                // "....."

		std::cout << str << std::endl;
	}

	void Append2()
	{
		std::string str;
		str.append("1").append("2").append("3").append("4").append("5").append("6");

		std::cout << str << std::endl;
	}

	void StartsWith()
	{
		std::string str{"1234567890"};
		std::cout << std::boolalpha << str.starts_with("123") << std::endl;
	}

	void ConcatStrings() {

		using namespace std::string_literals;

		std::string str1 = "Hello";
		std::string text = str1 + " World"; 

		std::cout << text << std::endl;

		const char kBrowserUIScheme[] = "browser";
		std::string str12 (std::string(kBrowserUIScheme) + "s" );

		std::cout << str12 << std::endl;


		std::string str3 = kBrowserUIScheme + " World"s;
		std::cout << str3 << std::endl;
	}

	void PushBack()
	{
		std::string str = "Initial";
		std::cout << str << std::endl;

		for (auto &c : " 12345") 
			str.push_back(c);
		
		std::cout << str << std::endl;
	}

	void PopBack()
	{
		const std::string base = "AAAA.BBBB.CCCC.DDDD.AAAA.DDDD.CCCC.AAAA.XXXX";
		std::cout << "Sample string: "  << base << std::endl;
		std::cout << "str.length() = "  << base.length() << std::endl;

		std::cout << "\n --------------------- TEST1:" << std::endl << std::endl;
		{
			std::string str(base);
			std::cout << str << std::endl;
			const auto size = str.length();
			for (int i = 1; i < size; i++)
			{
				str.pop_back();
				std::cout << str << std::endl;
			}
		}

		std::cout << "\n --------------------- TEST2:" << std::endl << std::endl;
		{
			std::string str(base);
			std::cout << str << std::endl;
			str.pop_back();
			std::cout << str << std::endl;
		}
	}

	void max_size()
	{
		std::string str("Test string");
		std::cout << "size: " << str.size() << "\n";
		std::cout << "length: " << str.length() << "\n";
		std::cout << "capacity: " << str.capacity() << "\n";
		std::cout << "max_size: " << str.max_size() << "\n";
	}

	void Replace() {
		std::string base = "Test1 Test2 Test3";
		std::string str2 = "Test4";

		{
			std::string str = base;
			str.replace(str.find("Test3"), str2.length(), str2);
			std::cout << str << std::endl;
		}

		{
			std::string str = base;
			str.replace(str.find("Test2"), str2.length(), str2, 0, 3);
			std::cout << str << std::endl;
		}
		/*
		{
			std::string str = "1/2/3/4/5";
			std::cout << str << std::endl;
		}
		*/
	}

	void Swap_First_and_Last_Chars() {
		std::string str = "a11111b";

		/*
		std::cout << str << std::endl;
		char back = str.back();
		str.back() = str.front();
		str.front() = back;
		*/
		std::swap(str.front(), str.back());

		std::cout << str << std::endl;
	}

	void Remove() {
		{
			std::string str = "AAA.BBB.CCCC.DDD.";
			str.erase(str.length() - 1);
			size_t pos = str.find_last_of(".");

			if (std::string::npos != pos) {
				str.erase(pos + 1);
			}

			std::cout << str << std::endl;
		}
		std::cout << "\nRemove char\n" << std::endl;
		{
			std::string str = "01234556789";
			str.erase(str.begin() + 3);
			std::cout << str << std::endl;
		}
	}

	void Find_Tests() {
		const std::string str = "AAAA.BBBB.CCCC.DDDD.AAAA.DDDD.CCCC.AAAA.XXXX";
		std::cout << "Sample string: " << str << std::endl;
		std::cout << "str.length() = " << str.length() << std::endl;

		{
			const std::string to_find("BBBB");
			std::cout << "\nfind (" << to_find << ") = " << str.find(to_find) << std::endl;
		}

		{
			const std::string to_find("CCCC");
			std::cout << "\nfind_first_of (" << to_find << ") = " << str.find_first_of(to_find) << std::endl;
		}

		{
			const std::string to_find("CCCC");
			std::cout << "\nfind_last_of (" << to_find << ") = " << str.find_last_of(to_find) << std::endl;
		}

		{
			std::string to_find("AA");
			std::cout << "\nfind_first_not_of (" << to_find << ") = " << str.find_first_not_of(to_find) << std::endl;
			to_find = "BB";
			std::cout << "find_first_not_of (" << to_find << ") = " << str.find_first_not_of(to_find) << std::endl;
		}

		{
			std::string to_find("AA");
			std::cout << "\nfind_last_not_of (" << to_find << ") = " << str.find_last_not_of(to_find) << std::endl;
			to_find = "NN";
			std::cout << "find_last_not_of (" << to_find << ") = " << str.find_last_not_of(to_find) << std::endl;
		}
	}

	void Insert() {
		std::string text = "abcc";
		std::string b_Str = "B";
		
		std::cout << text << std::endl;

		text.insert(std::next(text.begin()), b_Str.begin(), b_Str.end());

		std::cout << text << std::endl;
	}

	void SubString() {
		{
			std::string text = "0123456789";
			std::cout << text.substr(1, 5) << std::endl;
		}
	}

	//--------------------------------------------------------------------------------------------------------------//

	void consume(std::string&& str) {
		// std::string part(std::move(str));
		// std::string part(str.begin(), str.begin() + 5);

		std::string part(str.data());

		std::cout << "consume  : " << part << std::endl;
	}

	void SubString_Move() {
		std::string text = "0123456789";

		std::cout << "original : " << text << std::endl;

		consume(std::move(text));

		std::cout << "original : " << text << std::endl;
	}

	//--------------------------------------------------------------------------------------------------------------//

	void Trim(std::string& str) {
		size_t beg = 0, end = str.length();
		for (; end > beg & str[beg] == ' '; beg++) {}
		for (; end > 0 && str[--end] == ' '; ) {}
		str = str.substr(beg, end - beg + 1);
	}

	void Trim_2(std::string& str) {
		str.erase(0, str.find_first_not_of(' '));
		std::reverse(str.begin(), str.end());
		str.erase(0, str.find_first_not_of(' '));
		std::reverse(str.begin(), str.end());
	}

	void Trim() {
		const std::string base = "   Some   Sample    String  ";
		std::cout << "'" << base << "'" << std::endl << std::endl;

		std::cout << "-------------------------- Test 1:" << std::endl;
		{
			std::string str(base);
			Trim_2(str);
			std::cout << "'" << str << "'" << std::endl;
		}

		std::cout << "\n-------------------------- Test 2:" << std::endl;
		{
			std::string str(base);
			Trim(str);
			std::cout << "'" << str << "'" << std::endl;
		}
	}

	void Trim_Performance() {
		const std::string base = "   Some   Sample    String  ";
		
		{
			auto start = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < 10000; i++) {
				for (int n = 0; n < 10000; n++) {
					std::string str(base);
					Trim(str);
				}
			}
			auto end = std::chrono::high_resolution_clock::now();
			auto durtion = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
			std::cout << "Result: " << durtion << std::endl;
		}

		{
			auto start = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < 10000; i++) {
				for (int n = 0; n < 10000; n++) {
					std::string str(base);
					Trim_2(str);
				}
			}
			auto end = std::chrono::high_resolution_clock::now();
			auto durtion = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
			std::cout << "Result: " << durtion << std::endl;
		}
	}

	void Size_Storage() {
		std::string base;

		int i = 100;
		while (i--) {
			base.append("A");
			std::cout << "Size    : " << base.size() << std::endl;
			std::cout << "Capacity: " << base.capacity() << "\n" << std::endl;
		}
	}

	void Const_Lambda_Create() {
		//const int i = std::rand();
		const int i = 2;

		const std::string s = [&]() {
			switch (i % 4) {
			case 0:
				return "long string is mod 0";
			case 1:
				return "long string is mod 1";
			case 2:
				return "long string is mod 2";
			case 3:
				return "long string is mod 3";
			}
		}();

		std::cout << s << std::endl;
	}

	void Compare_String() {
		std::cout << "\n-------------------------- Test 1:" << std::endl;
		{
			std::string str1("qwerty"), str2("qwerty");
			bool result = (str1 == str2);
			

			std::cout << std::boolalpha <<  result << std::endl;
			std::cout << str1.compare(str2) << std::endl;
		}
		std::cout << "\n-------------------------- Test 2:" << std::endl;
		{
			std::string str1("qwerty"), str2("123231");
			bool result = (str1 == str2);

			std::cout << std::boolalpha << result << std::endl;
			std::cout << str1.compare(str2) << std::endl;
		}
		std::cout << "\n-------------------------- Test 3:" << std::endl;
		{
			std::string str1("1234"), str2("123456");
			bool result = (str1 == str2);

			std::cout << std::boolalpha << result << std::endl;
			std::cout << str1.compare(str2) << std::endl;
		}
		std::cout << "\n-------------------------- Test 4:" << std::endl;
		{
			std::string str1("1234"), str2("123456");
			bool result = (str1 == str2);

			std::cout << std::boolalpha << result << std::endl;
			std::cout << str1.compare(str2) << std::endl;
		}
	}

	void Split_String() {
		std::vector<std::string> parts;
		std::string text = "One__Two__Three__Four__Five";
		std::string delimiter = "__";

		size_t pos = 0, prev = 0;
		while ((pos = text.find(delimiter, prev)) != std::string::npos) {
			std::cout << text.substr(prev, pos - prev) << std::endl;
			prev = pos + delimiter.length();
		}
		std::cout << text.substr(prev, text.length()) << std::endl;
	}

	void Find_Middle() {
		std::string S = "abcd", R = "";
		size_t  mid = S.length() / 2 - (0 == S.length() % 2) ? 1 : 0;

		R.append(1, S[mid]);
		R += S.substr(0, mid);
		R += S.substr(mid + 1, S.length() - mid );

		std::cout << R << std::endl;

	}
};

namespace StringTests::Literals {

	void L_String() {
		std::wstring ws = L"hello world";

		// std::cout << ws.c_str() << std::endl;
	}

	std::string BuildScript(const std::string& href) {
		std::string script("element = Array.from(document.querySelectorAll('a'));");
		script += "element.find(el=>el.href.includes('";
		script += href;
		script += "'));";
		script += "if (element) element.click(); ";
		return script;
	}

	void R_String() {
		// A Normal string 
		std::string string1 = "Geeks.\nFor.\nGeeks.\n";
		// A Raw string 
		std::string string2 = R"(Geeks.\nFor.\nGeeks.\n)";

		std::cout << string1 << std::endl;
		std::cout << string2 << std::endl;


		std::cout << "\n-------------------------------------------- Str3 ---------------------------------" << std::endl;

		std::string string3 = R"(
			Hello
			World
			)";

		std::cout << string3<< std::endl;

		std::cout << "\n-------------------------------------------- Str4 ---------------------------------" << std::endl;

		std::string string4 = R"(Some/String\n)";
		std::cout << string4 << std::endl;


		std::string script(R"(const element = Array.from(document
			.querySelectorAll('a')).find(el=>el.href.includes(')");

		std::cout << script << std::endl;

		std::cout << BuildScript("TTTTT") << std::endl;
	}

	//////////////////

	void R_String_2() {
		std::string script(R"(
		array = Array.from(document.querySelectorAll('a')); 
		element = array.find(el=>el.href.includes('%s'));
		if (element) { 
			element.click();
		}
		)");

		std::cout << script << std::endl;
	}

	// -------------------------------------------------------------------------------//

	void R_String_ConstExpr() {

		/*
		constexpr auto jsv = R"({
			"feature-x-enabled": true,
			"value-of-y": 1729,
			"z-options": {"a": null,
			"b": "220 and 284",
			"c": [6, 28, 496]}
			})"_json;
		if constexpr (jsv["feature-x-enabled"]) {
			// code for feature x
		}
		else {
			// code when feature x turned off
		}*/
	}
}

namespace StringTests::Sprintf {

	template <typename ...Args>
	std::string stringWithFormat(const std::string& format, Args && ...args)
	{
		auto size = std::snprintf(nullptr, 0, format.c_str(), std::forward<Args>(args)...);
		std::string output(size + 1, '\0');
		std::sprintf(&output[0], format.c_str(), std::forward<Args>(args)...);
		return output;
	}

	template <typename ...Args>
	std::string stringWithFormatM(const std::string& format, Args && ...args)
	{
		// Calling std::snprintf with zero buf_size and null pointer for buffer 
		// is useful to determine the necessary buffer size to contain the output:
		auto size = std::snprintf(nullptr, 0, format.c_str(), std::forward<Args>(args)...);
		std::string output(size + 1, '\0');
		std::sprintf(output.data(), format.c_str(), std::forward<Args>(args)...);
		return std::move(output);
	}

	/////////////////////////////////////////////

	void Test() {
		char output[128] = {};
		int result = std::sprintf(output, "String: %s, Value: %d", "Str_Text", 123);
		std::cout << "result = " << result << std::endl;
		std::cout << "output: " << output << std::endl;
	}

	void Format_String_1() {
		std::string result = stringWithFormat("String: %s, Value: %d", "Str_Text", 123);
		std::cout << result << std::endl;
	}

	void Format_String_2() {
		std::string result = stringWithFormatM("String: %s, Value: %d", "Str_Text", 123);
		std::cout << result << std::endl;
	}

}

namespace StringTests::Utilities {

	void SplitString(const std::string& str,
					 std::vector<std::string>& cont,
					 const std::string& delimiter) {
		size_t pos = 0, prev = 0;
		while ((pos = str.find(delimiter, prev)) != std::string::npos) {
			cont.emplace_back(str.substr(prev, pos - prev));
			prev = pos + delimiter.length();
		}
		cont.emplace_back(str.substr(prev, pos - prev));
	}


	template <class Container>
	void split_string_2(const std::string& str, Container& cont) {
		std::istringstream iss(str);
		std::copy(std::istream_iterator<std::string>(iss),
			std::istream_iterator<std::string>(),
			std::back_inserter(cont));
	}

	void SwapChars(std::string text, const char src, const char dst) {
		std::transform(text.begin(), text.end(), text.begin(), [=](int c) { return src == c ? dst : c; });
	}
}

namespace StringTests::Performance_Tests {

	auto start = std::chrono::high_resolution_clock::now();
	auto end = std::chrono::high_resolution_clock::now();
	auto durtion = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

#define START_TIME_MEASURE start = std::chrono::high_resolution_clock::now();
#define STOP_TIME_MEASURE end = std::chrono::high_resolution_clock::now();
#define PRINT_RESULT(TEXT)  durtion = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count(); \
					  std::cout << TEXT <<" result: " << durtion << std::endl;

	void SplitTest() {
		std::string base = "11111111a 22222222222b 3333333333333c 4444444444d 55555555555f 6666666666666g";
		std::vector<std::string> parts;

		// Utilities::split_string_2(base, parts);
		// Utilities::split_string(base, parts, " ");

		unsigned int count = 0;

		START_TIME_MEASURE;
		for (int i = 0; i < 1000; i++) {
			for (int n = 0; n < 10000; n++) {
				parts.clear();
				// Utilities::split_string_2(base, parts);
				Utilities::SplitString(base, parts, " "); 
				count += parts.size();
			}
		}
		std::cout << count << std::endl;
		STOP_TIME_MEASURE;
		PRINT_RESULT("Result: ");
	}
}

namespace StringTests::Conversations {

	void From_Chars() {

		const char* str = "12 monkeys";
		int value;
		if (auto[ptr, ec] = std::from_chars(str, str + 10, value); ec != std::errc{}) {
			std::cout << "Errror" << std::endl;
		}

		std::cout << "Result: " << value << std::endl;
	}

	void From_Chars_ERROR() {

		const char* str = "#$#$onkeys";
		int value;
		if (auto[ptr, ec] = std::from_chars(str, str + 10, value); ec != std::errc{}) {
			std::cout << "Errror" << std::endl;
		}

		std::cout << "Result: " << value << std::endl;
	}

	void To_Chars() {
		int value = 42;
		char str[10];
		std::to_chars_result res = std::to_chars(str, str + 9, value);
		*res.ptr = '\0'; // ensure a trailing null character is behind

		std::cout << res.ptr << std::endl;
	}
}

//---------------------------------------------------------------------------------------------------//

namespace StringTests::RegEx {
	using namespace std::string_literals;

	// ^                - Start of string
	// [A-Z0-9._%+-]+   - At least one character in the range A-Z, 0-9, or one of -, %, + or - that represents the local part of the email address
	// @                - Character @
	// [A-Z0-9.-]+      - At least one character in the range A-Z, 0-9, or one of -, %, + or - that represents the hostname of the domain part
	// \.               - A dot that separates the domain hostname and label
	// [A-Z]{2,}        - The DNS label of a domain that can have between 2 and 63 characters
	// $                - End of the string

	bool is_valid_email_format_case_sensetive(std::string const & email) {
		constexpr auto pattern { R"(^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$)" };
		const auto rx = std::regex{ pattern , std::regex_constants::icase };
		return std::regex_match(email, rx);
	}

	bool is_valid_email_format(std::string const & email) {
		constexpr auto pattern{ R"(^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$)" };
		const auto rx = std::regex{ pattern};
		return std::regex_match(email, rx);
	}

	void run_test(const std::string& email) {
		std::cout << "Email: " << std::setw(25) << std::left << email << "  Match: "
			<< (is_valid_email_format_case_sensetive(email) ? "Valid" : "Invalid") << " (Case sensitive)" << std::endl;
		std::cout << "Email: " << std::setw(25) << std::left << email << "  Match: "
		    	  << (is_valid_email_format(email) ? "Valid" : "Invalid") << std::endl;
	}

	void EMail_Validator() {
		const std::vector<std::string> emailList{
			"JOHN.DOE@DOMAIN.COM",       // valid format
			"JOHNDOE@DOMAIL.CO.UK",      // valid format
			"JOHNDOE@DOMAIL.INFO",       // valid format
			"J.O.H.N_D.O.E@DOMAIN.INFO", // valid format
			"ROOT@LOCALHOST",            // invalid format
			"john.doe@domain.com"        // valid format
		};

		for (const auto& email : emailList)
			run_test(email);
	}

	//---------------------------------------------------//	

	const auto config { 
R"(
#remove # to uncomment the following lines
timeout = 120
server = 127.0.0.1
#retrycount=3
)"s };

	void Parsing_Test() {
		const auto pattern{ R"(^(?!#)(\w+)\s*=\s*([\w\d]+[\w\d._,\-:]*)$)"s };
		const auto rx = std::regex {pattern};

		auto match = std::smatch{};
		if (std::regex_search(config, match, rx)) {
			std::cout << match[1] << '=' << match[2] << std::endl;
		}
	}

	void Regex_Iterator() {
		const auto pattern{ R"(^(?!#)(\w+)\s*=\s*([\w\d]+[\w\d._,\-:]*)$)"s };
		const auto rx = std::regex{ pattern };

		auto end = std::sregex_iterator{};
		for (auto it = std::sregex_iterator{ std::begin(config), std::end(config), rx }; it != end; ++it){
			std::cout << "'"<< (*it)[1] << "' = '" << (*it)[2] << "'" << std::endl;
		}
	}

	void Regex_Token_Iterator() {
		const auto pattern{ R"(^(?!#)(\w+)\s*=\s*([\w\d]+[\w\d._,\-:]*)$)"s };
		const auto rx = std::regex{ pattern };

		auto end = std::sregex_token_iterator{};
		for (auto it = std::sregex_token_iterator{std::begin(config), std::end(config), rx };it != end; ++it) {
			std::cout << *it << std::endl;
		}
	}
}

namespace StringTests::Format {
	void Test() {

	 
	}
}

namespace StringTests::WString {

	void Create_Print() {
		const std::wstring szName = L"Global\\MyFileMappingObject";

		std::wcout << szName << std::endl;
	}
}


void StringTests::TEST_ALL(){
	// Create_Test();
	// Itearate();
	// ItearateBackward_RbeginRend();

	// Append();
	// Append2();

	// StartsWith();

	// Crbegin_Crend_Test();
	// PushBack();
	// PopBack();
	// max_size();
	// Replace();
	// Remove();
	// Swap_First_and_Last_Chars();
	// Find_Tests();
	// ConcatStrings();

	// SubString();
	SubString_Move();

	// Const_Lambda_Create();

	// Insert();

	// Trim();
	// Trim_Performance();

	// Size_Storage();

	// Literals::R_String();
	// Literals::R_String_2();
	// Literals::R_String_ConstExpr();
	// Literals::L_String();


	// Sprintf::Test();
	// Sprintf::Format_String_1();
	// Sprintf::Format_String_2();

	// Compare_String();

	// Split_String();

	// Performance_Tests::SplitTest();

	// Find_Middle();

	// Conversations::From_Chars();
	// Conversations::From_Chars_ERROR();
	// Conversations::To_Chars();

	// RegEx::EMail_Validator();
	// RegEx::Parsing_Test();
	// RegEx::Regex_Iterator();
	// RegEx::Regex_Token_Iterator();

	// Format::Test();

	// WString::Create_Print();
};