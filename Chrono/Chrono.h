//============================================================================
// Name        : Chrono.h
// Created on  : 31.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Chrono tests class 
//============================================================================

#ifndef CHRONO_TESTS_INCLUDE_GUARD__H
#define CHRONO_TESTS_INCLUDE_GUARD__H

namespace Chrono {
	void TEST_ALL();
};

#endif // !CHRONO_TESTS_INCLUDE_GUARD__H

