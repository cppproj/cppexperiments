//============================================================================
// Name        : Chrono.cpp
// Created on  : 31.05.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Chrono tests class 
//============================================================================

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include "Chrono.h"

namespace Chrono {

	void Duration_TimePoint_Print()
	{
		std::chrono::duration<int, std::ratio<60 * 60 * 24> > one_day(1);

		std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
		std::chrono::system_clock::time_point tomorrow = today + one_day;

		time_t time;

		time = std::chrono::system_clock::to_time_t(today);

		char str[26];
		ctime_s(str, sizeof str, &time);
		std::cout << "today is: " << str << std::endl;

		time = std::chrono::system_clock::to_time_t(tomorrow);
		ctime_s(str, sizeof str, &time);
		std::cout << "tomorrow will be: " << str << std::endl;
	}


	void Steady_clock()
	{
		using namespace std::chrono;
		steady_clock::time_point t1 = steady_clock::now();

		std::cout << "printing out 1000 stars..." << std::endl;
		for (int i = 0; i < 1000; ++i)
			std::cout << "*";
		std::cout << std::endl;

		steady_clock::time_point t2 = steady_clock::now();
		duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
		std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
	}

	void Duration() {
		auto d = std::chrono::seconds(100);
		std::cout << typeid(d).name() << std::endl;
	}

	void Clock_Test() {
		std::chrono::system_clock::time_point time_point_now = std::chrono::system_clock::now();
		time_t time = std::chrono::system_clock::to_time_t(time_point_now);


		char str[26];
		ctime_s(str, sizeof str, &time);
		std::cout << "today is: " << str << std::endl;
	}

	void Asctime()
	{
		std::time_t result = std::time(nullptr);
		std::cout << std::asctime(std::localtime(&result)) << result << " seconds since the Epoch" << std::endl;
	}

	void GM_time_VS_localtime()
	{
		std::time_t t = std::time(nullptr);
		std::cout << "UTC:       " << std::put_time(std::gmtime(&t), "%c %Z") << '\n';
		std::cout << "local:     " << std::put_time(std::localtime(&t), "%c %Z") << '\n';


		// POSIX-specific:
		/*
		std::string tz = "TZ=Asia/Singapore";
		putenv(tz.data());
		std::cout << "Singapore: " << std::put_time(std::localtime(&t), "%c %Z") << '\n';
		*/
	}

	void PrintLocaltime()
	{
		auto now = std::chrono::system_clock::now();
		auto stime = std::chrono::system_clock::to_time_t(now);
		auto ltime = std::localtime(&stime);
		std::cout << std::put_time(ltime, "%c") << std::endl;
	}

	void Time_T()
	{
		std::time_t result = std::time(nullptr);
		std::cout << std::ctime(&result);
	}

	void Localtime_TM()
	{
		time_t rawtime;
		tm* timeinfo;

		time(&rawtime);
		timeinfo = localtime(&rawtime);
		printf("Current local time and date: %s", asctime(timeinfo));

		std::cout << "Hour: " << timeinfo->tm_hour << std::endl;
		std::cout << "Min: " << timeinfo->tm_min << std::endl;
		std::cout << "Sec: " << timeinfo->tm_sec << std::endl;
	}

	void Zones_Tests()
	{
		time_t rawtime;
		tm* ptm;

		static constexpr int MST = -7;
		static constexpr int UTC = 0;
		static constexpr int CCT = 8;

		time(&rawtime);

		ptm = gmtime(&rawtime);

		puts("Current time around the World:");
		printf("Phoenix, AZ (U.S.) :  %2d:%02d\n", (ptm->tm_hour + MST) % 24, ptm->tm_min);
		printf("Reykjavik (Iceland) : %2d:%02d\n", (ptm->tm_hour + UTC) % 24, ptm->tm_min);
		printf("Beijing (China) :     %2d:%02d\n", (ptm->tm_hour + CCT) % 24, ptm->tm_min);
	}

	void TIme_Format() {

		//auto now = std::chrono::system_clock::now();
		//std::cout << date::format("%T", std::chrono::floor<std::chrono::milliseconds>(now));

		//auto now = std::chrono::system_clock::now();
		//auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch() % std::chrono::seconds{ 1 });
		//std::cout << std::date::format("%X", std::chrono::floor<std::chrono::milliseconds>(now)) << "," << ms.count();
	}
};


namespace Chrono {
	void TEST_ALL() {
		// Steady_clock();
		// Duration_TimePoint_Print();
		// Clock_Test();

		Duration();

		// Asctime();
		// GM_time_VS_localtime();
		// Time_T();

		// Zones_Tests();

		// Localtime_TM();

		// PrintLocaltime();
	}
};
