//============================================================================
// Name        : FilesStreams.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Files and streams tests
//============================================================================

#ifndef FILES_STREAMS_TESTS__H_
#define FILES_STREAMS_TESTS__H_

namespace FilesStreams {
	void TEST_ALL();
};

#endif /* FILES_STREAMS_TESTS__H_ */