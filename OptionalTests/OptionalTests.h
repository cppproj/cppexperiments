//============================================================================
// Name        : OptionalTests.h
// Created on  : 
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Optional tests
//============================================================================

#ifndef OPTIONAL_PARAMETERS_TESTS__H_
#define OPTIONAL_PARAMETERS_TESTS__H_

namespace OptionalParamsTests {
	void OptionalCreation();
	void OptionalCreation_Test2();
	void GetParamValue();
	void CheckParamValue();
	void ChangeOptParamValue();
	void CompareValues();
	void UserWithOptionalName();
	void ValueOR_Tests();
	void Options_ParseIntTest();

	void ChangeValues();

	void TEST_ALL();
};

#endif /* OPTIONAL_PARAMETERS_TESTS__H_ */
