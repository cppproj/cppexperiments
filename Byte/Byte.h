//============================================================================
// Name        : Byte.h
// Created on  : 02.06.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Byte tests
//============================================================================

#ifndef BYTE__INCLUDE_GUARD__H
#define BYTE__INCLUDE_GUARD__H

namespace Byte {
	void TEST_ALL();
};

#endif // (!BYTE__INCLUDE_GUARD__H)
